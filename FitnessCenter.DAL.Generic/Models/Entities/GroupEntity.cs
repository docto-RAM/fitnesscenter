﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class GroupEntity<TUser, TUserId> : BaseEntity<int>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required]
        [StringLength(128)]
        public string Name
        {
            get;
            set;
        }

        public int? MaxNumberOfPeople
        {
            get;
            set;
        }

        public decimal? Cost
        {
            get;
            set;
        }


        public virtual ICollection<ClientEntity<TUser, TUserId>> Clients
        {
            get;
            set;
        }

        public virtual ICollection<ScheduleEntity<TUser, TUserId>> Schedules
        {
            get;
            set;
        }
    }
}
