﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class ScheduleEntity<TUser, TUserId> : BaseEntity<int>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [ForeignKey("Trainer")]
        public int? TrainerId
        {
            get;
            set;
        }

        [Required]
        [ForeignKey("Group")]
        public int GroupId
        {
            get;
            set;
        }

        [Required]
        [ForeignKey("FitnessType")]
        public int FitnessTypeId
        {
            get;
            set;
        }

        [Required]
        public DateTime BeginTime
        {
            get;
            set;
        }

        [Required]
        public DateTime EndTime
        {
            get;
            set;
        }

        [StringLength(1024)]
        public string Place
        {
            get;
            set;
        }


        public virtual TrainerEntity<TUser, TUserId> Trainer
        {
            get;
            set;
        }

        public virtual GroupEntity<TUser, TUserId> Group
        {
            get;
            set;
        }

        public virtual FitnessTypeEntity<TUser, TUserId> FitnessType
        {
            get;
            set;
        }
    }
}
