﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class ClientEntity<TUser, TUserId> : BaseEntity<int>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required]
        [ForeignKey("User")]
        public TUserId UserId
        {
            get;
            set;
        }

        [Required]
        [ForeignKey("Group")]
        public int GroupId
        {
            get;
            set;
        }

        public DateTime? PaymentDate
        {
            get;
            set;
        }

        
        public virtual TUser User
        {
            get;
            set;
        }

        public virtual GroupEntity<TUser, TUserId> Group
        {
            get;
            set;
        }
    }
}
