﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class IncomingMessageEntity<TUser, TUserId> : BaseEntity<int>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required]
        public int OutgoingMessageId
        {
            get;
            set;
        }

        [Required]
        [ForeignKey("User")]
        public TUserId UserId
        {
            get;
            set;
        }

        public DateTime? ReadDate
        {
            get;
            set;
        }


        public virtual TUser User
        {
            get;
            set;
        }
    }
}
