﻿using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class FitnessTypeEntity<TUser, TUserId> : BaseEntity<int>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required]
        public FitnessTypeEnum Name
        {
            get;
            set;
        }


        public virtual ICollection<ScheduleEntity<TUser, TUserId>> Schedules
        {
            get;
            set;
        }
    }
}
