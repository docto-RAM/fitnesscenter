﻿using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class UserProfileEntity<TUser, TUserId> : BaseEntity<TUserId>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Key]
        [ForeignKey("User")]
        public override TUserId Id
        {
            get;
            set;
        }

        [Required]
        [StringLength(256)]
        public string Surname
        {
            get;
            set;
        }

        [Required]
        [StringLength(256)]
        public string Name
        {
            get;
            set;
        }

        [StringLength(256)]
        public string Patronymic
        {
            get;
            set;
        }

        [Required]
        public DateTime Birthday
        {
            get;
            set;
        }

        [Required]
        [StringLength(1024)]
        public string Address
        {
            get;
            set;
        }

        public byte[] Photo
        {
            get;
            set;
        }

        [Required]
        public LanguageEnum Language
        {
            get;
            set;
        }

        [Required]
        public bool IsBlocked
        {
            get;
            set;
        }


        public virtual TUser User
        {
            get;
            set;
        }
    }
}
