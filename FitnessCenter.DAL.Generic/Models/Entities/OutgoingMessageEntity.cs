﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class OutgoingMessageEntity<TUser, TUserId> : BaseEntity<int>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required]
        [ForeignKey("User")]
        public TUserId UserId
        {
            get;
            set;
        }

        [StringLength(256)]
        public string Title
        {
            get;
            set;
        }

        [StringLength(1024)]
        public string Text
        {
            get;
            set;
        }

        public DateTime? SentDate
        {
            get;
            set;
        }


        public virtual TUser User
        {
            get;
            set;
        }
    }
}
