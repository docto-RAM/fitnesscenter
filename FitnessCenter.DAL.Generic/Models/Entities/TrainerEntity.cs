﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FitnessCenter.DAL.Generic.Models.Entities
{
    public class TrainerEntity<TUser, TUserId> : BaseEntity<int>
        where TUser : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required]
        [ForeignKey("User")]
        public TUserId UserId
        {
            get;
            set;
        }

        [Required]
        public bool IsActive
        {
            get;
            set;
        }


        public virtual TUser User
        {
            get;
            set;
        }

        public virtual ICollection<ScheduleEntity<TUser, TUserId>> Schedules
        {
            get;
            set;
        }
    }
}
