﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using System;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.DAL.Generic.Models
{
    public abstract class BaseEntity<TEntityId> : IBaseEntity<TEntityId>
        where TEntityId : IComparable<TEntityId>
    {
        [Key]
        public virtual TEntityId Id
        {
            get;
            set;
        }
    }
}
