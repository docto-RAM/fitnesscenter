﻿using FitnessCenter.CrossCutting.Interfaces.BLL;
using System;

namespace FitnessCenter.BLL.Generic.Models
{
    public abstract class BaseDTO<TDTOId> : IBaseDTO<TDTOId>
        where TDTOId : IComparable<TDTOId>
    {
        public virtual TDTOId Id
        {
            get;
            set;
        }
    }
}
