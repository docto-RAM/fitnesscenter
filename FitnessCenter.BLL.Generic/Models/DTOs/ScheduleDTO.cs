﻿using FitnessCenter.CrossCutting.Enumerations;
using System;

namespace FitnessCenter.BLL.Generic.Models.DTOs
{
    public class ScheduleDTO<TUserId> : BaseDTO<int>
        where TUserId : IComparable<TUserId>
    {
        public int? TrainerId
        {
            get;
            set;
        }

        public string TrainerSurname
        {
            get;
            set;
        }

        public string TrainerName
        {
            get;
            set;
        }

        public string TrainerPatronymic
        {
            get;
            set;
        }

        public int GroupId
        {
            get;
            set;
        }

        public string GroupName
        {
            get;
            set;
        }

        public int? GroupMaxNumberOfPeopleInGroup
        {
            get;
            set;
        }

        public decimal? GroupCost
        {
            get;
            set;
        }

        public int FitnessTypeId
        {
            get;
            set;
        }

        public FitnessTypeEnum FitnessTypeName
        {
            get;
            set;
        }

        public DateTime BeginTime
        {
            get;
            set;
        }

        public DateTime EndTime
        {
            get;
            set;
        }

        public string Place
        {
            get;
            set;
        }
    }
}
