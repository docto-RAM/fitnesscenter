﻿using System;
using System.Collections.Generic;

namespace FitnessCenter.BLL.Generic.Models.DTOs
{
    public class GroupDTO<TUserId> : BaseDTO<int>
        where TUserId : IComparable<TUserId>
    {
        public string Name
        {
            get;
            set;
        }

        public int? MaxNumberOfPeople
        {
            get;
            set;
        }

        public decimal? Cost
        {
            get;
            set;
        }

        public ICollection<ClientDTO<TUserId>> Clients
        {
            get;
            set;
        }

        public ICollection<ScheduleDTO<TUserId>> Schedules
        {
            get;
            set;
        }
    }
}
