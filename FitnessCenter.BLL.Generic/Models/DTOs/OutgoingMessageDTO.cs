﻿using System;

namespace FitnessCenter.BLL.Generic.Models.DTOs
{
    public class OutgoingMessageDTO<TUserId> : BaseDTO<int>
        where TUserId : IComparable<TUserId>
    {
        public TUserId UserId
        {
            get;
            set;
        }

        public string UserSurname
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string UserPatronymic
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public DateTime? SentDate
        {
            get;
            set;
        }
    }
}
