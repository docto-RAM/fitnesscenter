﻿using System;

namespace FitnessCenter.BLL.Generic.Models.DTOs
{
    public class ClientDTO<TUserId> : BaseDTO<int>
        where TUserId : IComparable<TUserId>
    {
        public TUserId UserId
        {
            get;
            set;
        }

        public string UserSurname
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string UserPatronymic
        {
            get;
            set;
        }

        public DateTime UserBirthday
        {
            get;
            set;
        }

        public byte[] UserPhoto
        {
            get;
            set;
        }

        public bool UserIsBlocked
        {
            get;
            set;
        }

        public int GroupId
        {
            get;
            set;
        }

        public string GroupName
        {
            get;
            set;
        }

        public decimal? GroupCost
        {
            get;
            set;
        }

        public DateTime? PaymentDate
        {
            get;
            set;
        }
    }
}
