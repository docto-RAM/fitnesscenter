﻿using FitnessCenter.CrossCutting.Enumerations;
using System;

namespace FitnessCenter.BLL.Generic.Models.DTOs
{
    public class UserDTO<TUserId> : BaseDTO<TUserId>
        where TUserId : IComparable<TUserId>
    {
        public string Email
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string Surname
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Patronymic
        {
            get;
            set;
        }

        public DateTime Birthday
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public byte[] Photo
        {
            get;
            set;
        }

        public LanguageEnum Language
        {
            get;
            set;
        }

        public bool IsBlocked
        {
            get;
            set;
        }
    }
}
