﻿using System;
using System.Collections.Generic;

namespace FitnessCenter.BLL.Generic.Models.DTOs
{
    public class TrainerDTO<TUserId> : BaseDTO<int>
        where TUserId : IComparable<TUserId>
    {
        public TUserId UserId
        {
            get;
            set;
        }

        public string UserSurname
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string UserPatronymic
        {
            get;
            set;
        }

        public DateTime UserBirthday
        {
            get;
            set;
        }

        public string UserAddress
        {
            get;
            set;
        }

        public byte[] UserPhoto
        {
            get;
            set;
        }

        public bool UserIsBlocked
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public ICollection<ScheduleDTO<TUserId>> Schedules
        {
            get;
            set;
        }
    }
}
