﻿using System;

namespace FitnessCenter.BLL.Generic.Models.DTOs
{
    public class IncomingMessageDTO<TUserId> : BaseDTO<int>
        where TUserId : IComparable<TUserId>
    {
        public int OutgoingMessageId
        {
            get;
            set;
        }

        public TUserId UserId
        {
            get;
            set;
        }

        public string UserSurname
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string UserPatronymic
        {
            get;
            set;
        }

        public DateTime? ReadDate
        {
            get;
            set;
        }
    }
}
