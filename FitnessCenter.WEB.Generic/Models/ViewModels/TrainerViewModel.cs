﻿using FitnessCenter.CrossCutting.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.Generic.Models.ViewModels
{
    public class TrainerViewModel<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int Id
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public TUserId UserId
        {
            get;
            set;
        }

        [Display(Name = "Surname", ResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string UserSurname
        {
            get;
            set;
        }

        [Display(Name = "Name", ResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string UserName
        {
            get;
            set;
        }

        [Display(Name = "Patronymic", ResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string UserPatronymic
        {
            get;
            set;
        }

        [Display(Name = "Birthday", ResourceType = typeof(Resource))]
        public DateTime UserBirthday
        {
            get;
            set;
        }

        [Display(Name = "Address", ResourceType = typeof(Resource))]
        public string UserAddress
        {
            get;
            set;
        }

        public byte[] UserPhoto
        {
            get;
            set;
        }

        [Display(Name = "IsBlocked", ResourceType = typeof(Resource))]
        public bool UserIsBlocked
        {
            get;
            set;
        }

        [Display(Name = "IsActive", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public bool IsActive
        {
            get;
            set;
        }

        public ICollection<ScheduleViewModel<TUserId>> Schedules
        {
            get;
            set;
        }
    }
}
