﻿using FitnessCenter.CrossCutting.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.Generic.Models.ViewModels
{
    public class IncomingMessageViewModel<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int Id
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int OutgoingMessageId
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public TUserId UserId
        {
            get;
            set;
        }

        [Display(Name = "Surname", ResourceType = typeof(Resource))]
        public string UserSurname
        {
            get;
            set;
        }

        [Display(Name = "Name", ResourceType = typeof(Resource))]
        public string UserName
        {
            get;
            set;
        }

        [Display(Name = "Patronymic", ResourceType = typeof(Resource))]
        public string UserPatronymic
        {
            get;
            set;
        }

        [Display(Name = "ReadDate", ResourceType = typeof(Resource))]
        [DataType(DataType.DateTime)]
        public DateTime? ReadDate
        {
            get;
            set;
        }
    }
}
