﻿using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.Generic.Models.ViewModels
{
    public class UserViewModel<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public TUserId Id
        {
            get;
            set;
        }

        [Display(Name = "Email", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessageResourceName = "EmailAddressIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Email
        {
            get;
            set;
        }

        [Display(Name = "Password", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [RegularExpression(@"(?=^.{6,25}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessageResourceName = "PasswordIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }

        [Display(Name = "Surname", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Surname
        {
            get;
            set;
        }

        [Display(Name = "Name", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Name
        {
            get;
            set;
        }

        [Display(Name = "Patronymic", ResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Patronymic
        {
            get;
            set;
        }

        [Display(Name = "Birthday", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        public DateTime Birthday
        {
            get;
            set;
        }

        [Display(Name = "Address", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(1024, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Address
        {
            get;
            set;
        }

        [Display(Name = "Photo", ResourceType = typeof(Resource))]
        public byte[] Photo
        {
            get;
            set;
        }

        [Display(Name = "Language", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public LanguageEnum Language
        {
            get;
            set;
        }

        [Display(Name = "IsBlocked", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public bool IsBlocked
        {
            get;
            set;
        }
    }
}
