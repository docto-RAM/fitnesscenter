﻿using FitnessCenter.CrossCutting.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.Generic.Models.ViewModels
{
    public class OutgoingMessageViewModel<TUserId>
        where TUserId : IComparable<TUserId>
    {
        public int Id
        {
            get;
            set;
        }

        public TUserId UserId
        {
            get;
            set;
        }

        [Display(Name = "Surname", ResourceType = typeof(Resource))]
        public string UserSurname
        {
            get;
            set;
        }

        [Display(Name = "Name", ResourceType = typeof(Resource))]
        public string UserName
        {
            get;
            set;
        }

        [Display(Name = "Patronymic", ResourceType = typeof(Resource))]
        public string UserPatronymic
        {
            get;
            set;
        }

        [Display(Name = "Subject", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Title
        {
            get;
            set;
        }

        [Display(Name = "Message", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(1024, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Text
        {
            get;
            set;
        }

        [Display(Name = "SentDate", ResourceType = typeof(Resource))]
        [DataType(DataType.DateTime)]
        public DateTime? SentDate
        {
            get;
            set;
        }
    }
}
