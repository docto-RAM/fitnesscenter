﻿using FitnessCenter.CrossCutting.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.Generic.Models.ViewModels
{
    public class ClientViewModel<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int Id
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public TUserId UserId
        {
            get;
            set;
        }

        [Display(Name = "Surname", ResourceType = typeof(Resource))]
        public string UserSurname
        {
            get;
            set;
        }

        [Display(Name = "Name", ResourceType = typeof(Resource))]
        public string UserName
        {
            get;
            set;
        }

        [Display(Name = "Patronymic", ResourceType = typeof(Resource))]
        public string UserPatronymic
        {
            get;
            set;
        }

        [Display(Name = "Birthday", ResourceType = typeof(Resource))]
        public DateTime UserBirthday
        {
            get;
            set;
        }

        public byte[] UserPhoto
        {
            get;
            set;
        }

        [Display(Name = "IsBlocked", ResourceType = typeof(Resource))]
        public bool UserIsBlocked
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int GroupId
        {
            get;
            set;
        }

        [Display(Name = "Group name", ResourceType = typeof(Resource))]
        [StringLength(128, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string GroupName
        {
            get;
            set;
        }

        [Display(Name = "GroupCost", ResourceType = typeof(Resource))]
        [DataType(DataType.Currency)]
        public decimal? GroupCost
        {
            get;
            set;
        }

        [Display(Name = "IsPaid", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public DateTime? PaymentDate
        {
            get;
            set;
        }
    }
}
