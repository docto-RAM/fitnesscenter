﻿using FitnessCenter.CrossCutting.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.Generic.Models.ViewModels
{
    public class GroupViewModel<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int Id
        {
            get;
            set;
        }

        [Display(Name = "GroupName", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(128, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Name
        {
            get;
            set;
        }

        [Display(Name = "MaxNumberOfPeople", ResourceType = typeof(Resource))]
        public int? MaxNumberOfPeople
        {
            get;
            set;
        }

        [Display(Name = "Cost", ResourceType = typeof(Resource))]
        [DataType(DataType.Currency)]
        public decimal? Cost
        {
            get;
            set;
        }

        public ICollection<ClientViewModel<TUserId>> Clients
        {
            get;
            set;
        }

        public ICollection<ScheduleViewModel<TUserId>> Schedules
        {
            get;
            set;
        }
    }
}
