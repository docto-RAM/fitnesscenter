﻿using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.Generic.Models.ViewModels
{
    public class ScheduleViewModel<TUserId>
        where TUserId : IComparable<TUserId>
    {
        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int Id
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int? TrainerId
        {
            get;
            set;
        }

        [Display(Name = "Surname", ResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string TrainerSurname
        {
            get;
            set;
        }

        [Display(Name = "Name", ResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string TrainerName
        {
            get;
            set;
        }

        [Display(Name = "Patronymic", ResourceType = typeof(Resource))]
        [StringLength(256, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string TrainerPatronymic
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int GroupId
        {
            get;
            set;
        }

        [Display(Name = "GroupName", ResourceType = typeof(Resource))]
        [StringLength(128, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string GroupName
        {
            get;
            set;
        }

        [Display(Name = "MaxNumberOfPeople", ResourceType = typeof(Resource))]
        public int? GroupMaxNumberOfPeopleInGroup
        {
            get;
            set;
        }

        [Display(Name = "Cost", ResourceType = typeof(Resource))]
        [DataType(DataType.Currency)]
        public decimal? GroupCost
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "DataIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public int FitnessTypeId
        {
            get;
            set;
        }

        [Display(Name = "FitnessType", ResourceType = typeof(Resource))]
        public FitnessTypeEnum FitnessTypeName
        {
            get;
            set;
        }

        [Display(Name = "BeginTime", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public DateTime BeginTime
        {
            get;
            set;
        }

        [Display(Name = "EndTime", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        public DateTime EndTime
        {
            get;
            set;
        }

        [Display(Name = "Place", ResourceType = typeof(Resource))]
        [StringLength(1024, ErrorMessageResourceName = "TheLengthOfTheFieldIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Place
        {
            get;
            set;
        }
    }
}
