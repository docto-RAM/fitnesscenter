﻿using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework.Entities;
using System;
using static System.DateTime;

namespace FitnessCenter.BLL.BusinessLogic.Services
{
    public class IncomingMessageService : BaseService<IncomingMessageDTO, IncomingMessageEntity, int>, IIncomingMessageService<IncomingMessageDTO>
    {
        protected override Func<IncomingMessageDTO, IncomingMessageDTO, bool> IsUnique => (x, y) => x.Id == y.Id;
        protected override Func<IncomingMessageDTO, IncomingMessageDTO, bool> IsExists => (x, y) => x.Id == y.Id;

        public IncomingMessageService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        protected override void Validate(IncomingMessageDTO incomingMessageDTO)
        {
            base.Validate(incomingMessageDTO);

            if (incomingMessageDTO.ReadDate.HasValue && Now < incomingMessageDTO.ReadDate.Value)
            {
                throw new ValidationException(nameof(incomingMessageDTO.ReadDate), Resource.ReadDateIsInvalid);
            }
        }
    }
}
