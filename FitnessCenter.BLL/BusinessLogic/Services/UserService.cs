﻿using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Ancillary;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework.Entities;
using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.UnitOfWork.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static System.String;

namespace FitnessCenter.BLL.BusinessLogic.Services
{
    public class UserService : BaseService<UserDTO, UserEntity, string>, IUserService<UserDTO, string>
    {
        private readonly IRepository<UserProfileEntity, string> userProfileRepository;

        protected override Func<UserDTO, UserDTO, bool> IsUnique => (x, y) => x.Id == y.Id;
        protected override Func<UserDTO, UserDTO, bool> IsExists => (x, y) => x.Email == y.Email;

        public UserService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            userProfileRepository = this.unitOfWork.GetRepository<UserProfileEntity, string>();
        }

        public ICollection<UserDTO> GetAll()
        {
            var userDTOs = new List<UserDTO>();
            foreach (var user in repository.GetAll())
            {
                userDTOs.Add(new UserDTO().MapFrom(user));
            }
            return userDTOs;
        }

        public IdentityResult UpdateSecurityStamp(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            UserEntity user = ((UserRepository)repository).FindById(userDTO.Id);
            return (user != null) ? ((UserRepository)repository).UpdateSecurityStamp(user.Id) : null;
        }

        public async Task<IdentityResult> UpdateSecurityStampAsync(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            UserEntity user = await ((UserRepository)repository).FindByIdAsync(userDTO.Id);
            return (user != null) ? await ((UserRepository)repository).UpdateSecurityStampAsync(user.Id) : null;
        }

        public ClaimsIdentity Authenticate(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            UserEntity user = (IsNullOrEmpty(userDTO.Id))
                ? ((UserRepository)repository).Find(userDTO.Email, userDTO.Password)
                :((UserRepository)repository).FindById(userDTO.Id);

            return (user != null) ? ((UserRepository)repository).CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie) : null;
        }

        public async Task<ClaimsIdentity> AuthenticateAsync(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            UserEntity user = (IsNullOrEmpty(userDTO.Id))
                ? await ((UserRepository)repository).FindAsync(userDTO.Email, userDTO.Password)
                : await ((UserRepository)repository).FindByIdAsync(userDTO.Id);

            return (user != null) ? await ((UserRepository)repository).CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie) : null;
        }

        public bool AddToRole(UserDTO userDTO, RoleEnum role) => Perform(((UserRepository)repository).AddToRole, userDTO, role);

        public async Task<bool> AddToRoleAsync(UserDTO userDTO, RoleEnum role) => await PerformAsync(((UserRepository)repository).AddToRoleAsync, userDTO, role);

        public IEnumerable<RoleEnum> GetRoles(UserDTO userDTO) => Perform(((UserRepository)repository).GetRoles, userDTO);

        public async Task<IEnumerable<RoleEnum>> GetRolesAsync(UserDTO userDTO) => await PerformAsync(((UserRepository)repository).GetRolesAsync, userDTO);

        public bool IsInRole(UserDTO userDTO, RoleEnum role) => Perform(((UserRepository)repository).IsInRole, userDTO, role);

        public async Task<bool> IsInRoleAsync(UserDTO userDTO, RoleEnum role) => await PerformAsync(((UserRepository)repository).IsInRoleAsync, userDTO, role);

        public bool RemoveFromRole(UserDTO userDTO, RoleEnum role) => Perform(((UserRepository)repository).RemoveFromRole, userDTO, role);

        public async Task<bool> RemoveFromRoleAsync(UserDTO userDTO, RoleEnum role) => await PerformAsync(((UserRepository)repository).RemoveFromRoleAsync, userDTO, role);

        public override UserDTO Find(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            UserEntity user = ((UserRepository)repository).FindByEmail(userDTO.Email);
            return (user != null) ? new UserDTO().MapFrom(user) : null;
        }

        public override async Task<UserDTO> FindAsync(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            UserEntity user = await ((UserRepository)repository).FindByEmailAsync(userDTO.Email);
            return (user != null) ? new UserDTO().MapFrom(user) : null;
        }

        public override OperationDetails Create(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            Validate(userDTO);

            bool? isItemExists = Find(x => IsExists(x, userDTO))?.Count > 0;
            if (isItemExists.HasValue && isItemExists.Value)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return Perform(((UserRepository)repository).Create, userDTO, userDTO.Password);
        }

        public override async Task<OperationDetails> CreateAsync(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            Validate(userDTO);

            bool? isItemExists = Find(x => IsExists(x, userDTO))?.Count > 0;
            if (isItemExists.HasValue && isItemExists.Value)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return await PerformAsync(((UserRepository)repository).CreateAsync, userDTO, userDTO.Password);
        }

        protected override OperationDetails Perform(Func<UserEntity, bool> func, UserDTO userDTO, UserEntity userEntity)
        {
            bool isSuccessful = func(userDTO.MapTo(userEntity ?? new UserEntity()));
            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }
            unitOfWork.Save();

            return PerformForUserProfile(func.Method.Name, userDTO);
        }

        protected override async Task<OperationDetails> PerformAsync(Func<UserEntity, Task<bool>> func, UserDTO userDTO, UserEntity userEntity)
        {
            bool isSuccessful = await func(userDTO.MapTo(userEntity ?? new UserEntity()));
            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }
            await unitOfWork.SaveAsync();

            return await PerformForUserProfileAsync(func.Method.Name, userDTO);
        }

        protected override void Validate(UserDTO userDTO)
        {
            base.Validate(userDTO);

            if (userDTO.Birthday < new DateTime(1900, 1, 1))
            {
                throw new ValidationException(nameof(userDTO.Birthday), Resource.BirthdayIsInvalid);
            }

            if (userDTO.Language <= LanguageEnum.Default || LanguageEnum.DefaultLimit <= userDTO.Language)
            {
                throw new ValidationException(nameof(userDTO.Language), Resource.LanguageIsInvalid);
            }
        }

        private IEnumerable<RoleEnum> Perform(Func<string, IEnumerable<string>> func, UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            Validate(userDTO);

            return func(userDTO.Id).Select(x => (RoleEnum)Enum.Parse(typeof(RoleEnum), x));
        }

        private async Task<IEnumerable<RoleEnum>> PerformAsync(Func<string, Task<IEnumerable<string>>> func, UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            Validate(userDTO);

            var result = await func(userDTO.Id);

            return result.Select(x => (RoleEnum)Enum.Parse(typeof(RoleEnum), x));
        }

        private bool Perform(Func<string, string, bool> func, UserDTO userDTO, RoleEnum role)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            Validate(userDTO);

            if (role <= RoleEnum.Default || RoleEnum.DefaultLimit <= role)
            {
                throw new ArgumentException(nameof(role));
            }

            return func(userDTO.Id, role.ToString());
        }

        private async Task<bool> PerformAsync(Func<string, string, Task<bool>> func, UserDTO userDTO, RoleEnum role)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException(nameof(userDTO));
            }

            Validate(userDTO);

            if (role <= RoleEnum.Default || RoleEnum.DefaultLimit <= role)
            {
                throw new ArgumentException(nameof(role));
            }

            return await func(userDTO.Id, role.ToString());
        }

        private OperationDetails Perform<T>(Func<UserEntity, T, bool> func, UserDTO userDTO, T parameter)
        {
            UserDTO newUserDTO = userDTO;
            UserEntity user = new UserEntity();
            if(IsNullOrEmpty(newUserDTO.Id))
            {
                newUserDTO.Id = user.Id;
            }
            bool isSuccessful = func(newUserDTO.MapTo(user), parameter);
            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }
            unitOfWork.Save();

            return PerformForUserProfile(func.Method.Name, newUserDTO);
        }

        private async Task<OperationDetails> PerformAsync<T>(Func<UserEntity, T, Task<bool>> func, UserDTO userDTO, T parameter)
        {
            UserDTO newUserDTO = userDTO;
            UserEntity user = new UserEntity();
            if (IsNullOrEmpty(newUserDTO.Id))
            {
                newUserDTO.Id = user.Id;
            }
            bool isSuccessful = await func(newUserDTO.MapTo(user), parameter);
            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }
            await unitOfWork.SaveAsync();

            return await PerformForUserProfileAsync(func.Method.Name, newUserDTO);
        }

        private OperationDetails PerformForUserProfile(string methodName, UserDTO userDTO)
        {
            UserProfileEntity userProfile = null;
            bool isSuccessful = false;

            switch (methodName)
            {
                case nameof(repository.Create):
                    userProfile = CreateUserProfile(userDTO);
                    isSuccessful = userProfileRepository.Create(userProfile);
                    break;
                case nameof(repository.Update):
                    userProfile = CreateUserProfile(userDTO);
                    isSuccessful = userProfileRepository.Update(userProfile);
                    break;
                case nameof(repository.Delete):
                    return new OperationDetails(true, Resource.OperationSuccessful);
                default:
                    break;
            }

            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            unitOfWork.Save();

            return new OperationDetails(true, Resource.OperationSuccessful);
        }

        private async Task<OperationDetails> PerformForUserProfileAsync(string methodName, UserDTO userDTO)
        {
            UserProfileEntity userProfile = null;
            bool isSuccessful = false;

            switch (methodName)
            {
                case nameof(repository.CreateAsync):
                    userProfile = await CreateUserProfileAsync(userDTO);
                    isSuccessful = await userProfileRepository.CreateAsync(userProfile);
                    break;
                case nameof(repository.UpdateAsync):
                    userProfile = await CreateUserProfileAsync(userDTO);
                    isSuccessful = await userProfileRepository.UpdateAsync(userProfile);
                    break;
                case nameof(repository.DeleteAsync):
                    return new OperationDetails(true, Resource.OperationSuccessful);
                default:
                    break;
            }

            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            await unitOfWork.SaveAsync();

            return new OperationDetails(true, Resource.OperationSuccessful);
        }

        private UserProfileEntity CreateUserProfile(UserDTO userDTO)
        {
            UserEntity user = ((UserRepository)repository).FindByEmail(userDTO.Email);
            if (user == null)
            {
                throw new UnexpectedResultException(Resource.InternalServerError500);
            }
            userDTO.Id = user.Id;
            return userDTO.MapTo(new UserProfileEntity());
        }

        private async Task<UserProfileEntity> CreateUserProfileAsync(UserDTO userDTO)
        {
            UserEntity user = await ((UserRepository)repository).FindByEmailAsync(userDTO.Email);
            if (user == null)
            {
                throw new UnexpectedResultException(Resource.InternalServerError500);
            }
            userDTO.Id = user.Id;
            return userDTO.MapTo(new UserProfileEntity());
        }
    }
}
