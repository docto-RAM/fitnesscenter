﻿using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.DAL.EntityFramework.Entities;
using System;
using System.Collections.Generic;

namespace FitnessCenter.BLL.BusinessLogic.Services
{
    public class TrainerService : BaseService<TrainerDTO, TrainerEntity, int>, ITrainerService<TrainerDTO>
    {
        protected override Func<TrainerDTO, TrainerDTO, bool> IsUnique => (x, y) => x.Id == y.Id;
        protected override Func<TrainerDTO, TrainerDTO, bool> IsExists => (x, y) => x.UserId == y.UserId;

        public TrainerService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public ICollection<TrainerDTO> GetAll()
        {
            var trainerDTOs = new List<TrainerDTO>();
            foreach (var trainer in repository.GetAll())
            {
                trainerDTOs.Add(new TrainerDTO().MapFrom(trainer));
            }
            return trainerDTOs;
        }
    }
}
