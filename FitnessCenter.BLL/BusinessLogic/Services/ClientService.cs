﻿using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.DAL.EntityFramework.Entities;
using System;
using System.Collections.Generic;

namespace FitnessCenter.BLL.BusinessLogic.Services
{
    public class ClientService : BaseService<ClientDTO, ClientEntity, int>, IClientService<ClientDTO>
    {
        protected override Func<ClientDTO, ClientDTO, bool> IsUnique => (x, y) => x.Id == y.Id;
        protected override Func<ClientDTO, ClientDTO, bool> IsExists => (x, y) =>
        {
            return
                x.UserId == y.UserId &&
                x.GroupId == y.GroupId;
        };

        public ClientService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public ICollection<ClientDTO> GetAll()
        {
            var clientDTOs = new List<ClientDTO>();
            foreach (var client in repository.GetAll())
            {
                clientDTOs.Add(new ClientDTO().MapFrom(client));
            }

            return clientDTOs;
        }
    }
}
