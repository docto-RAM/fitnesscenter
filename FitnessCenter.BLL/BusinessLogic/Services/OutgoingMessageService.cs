﻿using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework.Entities;
using System;
using static System.DateTime;

namespace FitnessCenter.BLL.BusinessLogic.Services
{
    public class OutgoingMessageService : BaseService<OutgoingMessageDTO, OutgoingMessageEntity, int>, IOutgoingMessageService<OutgoingMessageDTO>
    {
        protected override Func<OutgoingMessageDTO, OutgoingMessageDTO, bool> IsUnique => (x, y) => x.Id == y.Id;
        protected override Func<OutgoingMessageDTO, OutgoingMessageDTO, bool> IsExists => (x, y) => x.Id == y.Id;

        public OutgoingMessageService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        protected override void Validate(OutgoingMessageDTO outgoingMessageDTO)
        {
            base.Validate(outgoingMessageDTO);

            if (outgoingMessageDTO.SentDate.HasValue && (outgoingMessageDTO.SentDate.Value < new DateTime(1900, 1, 1) || Now < outgoingMessageDTO.SentDate.Value))
            {
                throw new ValidationException(nameof(outgoingMessageDTO.SentDate), Resource.SentDateIsInvalid);
            }
        }
    }
}
