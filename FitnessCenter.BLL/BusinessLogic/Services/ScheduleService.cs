﻿using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.DateTime;

namespace FitnessCenter.BLL.BusinessLogic.Services
{
    public class ScheduleService : BaseService<ScheduleDTO, ScheduleEntity, int>, IScheduleService<ScheduleDTO>
    {
        protected override Func<ScheduleDTO, ScheduleDTO, bool> IsUnique => (x, y) => x.Id == y.Id;
        protected override Func<ScheduleDTO, ScheduleDTO, bool> IsExists => (x, y) =>
        {
            return
                (
                    (x.BeginTime <= y.BeginTime && x.EndTime >= y.BeginTime) ||
                    (x.BeginTime <= y.EndTime && x.EndTime >= y.EndTime)
                ) &&
                x.Place == y.Place;
        };

        public ScheduleService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public ICollection<ScheduleDTO> GetAll()
        {
            var scheduleDTOs = new List<ScheduleDTO>();
            foreach (var schedule in repository.GetAll())
            {
                scheduleDTOs.Add(new ScheduleDTO().MapFrom(schedule));
            }

            return scheduleDTOs;
        }

        public ICollection<ScheduleDTO> GetAllActual()
        {
            var scheduleDTOs = new List<ScheduleDTO>();
            foreach (var schedule in repository.GetAll()?.Where(x => x.BeginTime >= Now))
            {
                scheduleDTOs.Add(new ScheduleDTO().MapFrom(schedule));
            }

            return scheduleDTOs;
        }

        protected override void Validate(ScheduleDTO scheduleDTO)
        {
            base.Validate(scheduleDTO);

            if (scheduleDTO.FitnessTypeName <= FitnessTypeEnum.Default || FitnessTypeEnum.DefaultLimit <= scheduleDTO.FitnessTypeName)
            {
                throw new ValidationException(nameof(scheduleDTO.FitnessTypeName), Resource.FitnessTypeIsInvalid);
            }

            if (scheduleDTO.BeginTime < new DateTime(1900, 1, 1))
            {
                throw new ValidationException(nameof(scheduleDTO.BeginTime), Resource.BeginTimeIsInvalid);
            }

            if (scheduleDTO.EndTime < scheduleDTO.BeginTime)
            {
                throw new ValidationException(nameof(scheduleDTO.EndTime), Resource.EndTimeIsInvalid);
            }
        }
    }
}