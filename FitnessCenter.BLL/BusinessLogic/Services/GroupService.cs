﻿using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.DateTime;

namespace FitnessCenter.BLL.BusinessLogic.Services
{
    public class GroupService : BaseService<GroupDTO, GroupEntity, int>, IGroupService<GroupDTO>
    {
        protected override Func<GroupDTO, GroupDTO, bool> IsUnique => (x, y) => x.Id == y.Id;
        protected override Func<GroupDTO, GroupDTO, bool> IsExists => (x, y) => x.Name == y.Name;

        public GroupService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public ICollection<GroupDTO> GetAll()
        {
            var groupDTOs = new List<GroupDTO>();
            foreach (var group in repository.GetAll())
            {
                groupDTOs.Add(new GroupDTO().MapFrom(group));
            }

            return groupDTOs;
        }

        public ICollection<GroupDTO> GetAllActual()
        {
            var groupDTOs = new List<GroupDTO>();
            foreach (var group in repository.GetAll()?.Where(x => x.Schedules.Any(y => y.EndTime > Now)))
            {
                groupDTOs.Add(new GroupDTO().MapFrom(group));
            }

            return groupDTOs;
        }

        protected override void Validate(GroupDTO groupDTO)
        {
            base.Validate(groupDTO);

            if (groupDTO.MaxNumberOfPeople <= 0)
            {
                throw new ValidationException(nameof(groupDTO.MaxNumberOfPeople), Resource.MaxNumberOfPeopleValueIsInvalid);
            }

            if (groupDTO.Cost < 0M)
            {
                throw new ValidationException(nameof(groupDTO.Cost), Resource.CostIsInvalid);
            }
        }
    }
}
