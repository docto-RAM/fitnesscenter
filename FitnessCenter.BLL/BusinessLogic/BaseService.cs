﻿using FitnessCenter.CrossCutting.Ancillary;
using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.CrossCutting.Resources;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FitnessCenter.BLL.BusinessLogic
{
    public abstract class BaseService<TDTO, TEntity, TId> : IBaseService<TDTO, TId>
        where TDTO : class, IBaseDTO<TId>, IMapper<TEntity, TDTO>, new()
        where TEntity : class, IBaseEntity<TId>, new()
        where TId : IComparable<TId>
    {
        protected readonly IUnitOfWork unitOfWork;
        protected readonly IRepository<TEntity, TId> repository;

        protected abstract Func<TDTO, TDTO, bool> IsUnique
        {
            get;
        }
        protected abstract Func<TDTO, TDTO, bool> IsExists
        {
            get;
        }

        public BaseService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            repository = this.unitOfWork.GetRepository<TEntity, TId>();
        }

        public virtual TDTO Find(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            var item = repository.FindById(itemDTO.Id);
            if (item == null)
            {
                throw new UnexpectedResultException(Resource.NotFound404);
            }

            return new TDTO().MapFrom(item);
        }

        public virtual async Task<TDTO> FindAsync(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            var item = await repository.FindByIdAsync(itemDTO.Id);
            if (item == null)
            {
                throw new UnexpectedResultException(Resource.NotFound404);
            }

            return new TDTO().MapFrom(item);
        }

        public virtual ICollection<TDTO> Find(Func<TDTO, bool> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            var itemDTOs = new List<TDTO>();
            foreach (var item in repository.Find(x => predicate(new TDTO().MapFrom(x))))
            {
                itemDTOs.Add(new TDTO().MapFrom(item));
            }

            return itemDTOs;
        }

        public virtual OperationDetails Create(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            bool? isItemExists = Find(x => IsExists(x, itemDTO))?.Count > 0;
            if (isItemExists.HasValue && isItemExists.Value)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return Perform(repository.Create, itemDTO);
        }

        public virtual async Task<OperationDetails> CreateAsync(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            bool? isItemExists = Find(x => IsExists(x, itemDTO))?.Count > 0;
            if (isItemExists.HasValue && isItemExists.Value)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return await PerformAsync(repository.CreateAsync, itemDTO);
        }

        public virtual OperationDetails Update(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            bool? isItemUnique = Find(x => IsUnique(x, itemDTO))?.Count == 1;
            if (!isItemUnique.HasValue || !isItemUnique.Value)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return Perform(repository.Update, itemDTO, repository.FindById(itemDTO.Id));
        }

        public virtual async Task<OperationDetails> UpdateAsync(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            bool? isItemUnique = Find(x => IsUnique(x, itemDTO))?.Count == 1;
            if (!isItemUnique.HasValue || !isItemUnique.Value)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return await PerformAsync(repository.UpdateAsync, itemDTO, repository.FindById(itemDTO.Id));
        }

        public virtual OperationDetails Delete(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            var item = repository.FindById(itemDTO.Id);
            if (item == null)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return Perform(repository.Delete, new TDTO().MapFrom(item));
        }

        public virtual async Task<OperationDetails> DeleteAsync(TDTO itemDTO)
        {
            if (itemDTO == null)
            {
                throw new ArgumentNullException(nameof(itemDTO));
            }

            Validate(itemDTO);

            var item = await repository.FindByIdAsync(itemDTO.Id);
            if (item == null)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }

            return await PerformAsync(repository.DeleteAsync, new TDTO().MapFrom(item));
        }

        public virtual void Dispose()
        {
            unitOfWork.Dispose();
        }

        protected virtual OperationDetails Perform(Func<TEntity, bool> func, TDTO itemDTO, TEntity entity = null)
        {
            bool isSuccessful = func(itemDTO.MapTo(entity ?? new TEntity()));
            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }
            unitOfWork.Save();

            return new OperationDetails(true, Resource.OperationSuccessful);
        }

        protected virtual async Task<OperationDetails> PerformAsync(Func<TEntity, Task<bool>> func, TDTO itemDTO, TEntity entity = null)
        {
            bool isSuccessful = await func(itemDTO.MapTo(entity ?? new TEntity()));
            if (!isSuccessful)
            {
                return new OperationDetails(false, Resource.OperationUnsuccessful);
            }
            await unitOfWork.SaveAsync();

            return new OperationDetails(true, Resource.OperationSuccessful);
        }

        protected virtual void Validate(TDTO itemDTO)
        {
        }
    }
}