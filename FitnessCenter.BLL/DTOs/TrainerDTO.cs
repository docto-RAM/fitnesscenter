﻿using FitnessCenter.BLL.Generic.Models.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.DAL.EntityFramework.Entities;
using System.Linq;

namespace FitnessCenter.BLL.DTOs
{
    public class TrainerDTO : TrainerDTO<string>, IMapper<TrainerEntity, TrainerDTO>
    {
        public TrainerDTO MapFrom(TrainerEntity resource)
        {
            Id = resource.Id;
            UserId = resource.UserId;
            UserSurname = resource.User.UserProfile.Surname;
            UserName = resource.User.UserProfile.Name;
            UserPatronymic = resource.User.UserProfile.Patronymic;
            UserBirthday = resource.User.UserProfile.Birthday;
            UserAddress = resource.User.UserProfile.Address;
            UserPhoto = resource.User.UserProfile.Photo;
            UserIsBlocked = resource.User.UserProfile.IsBlocked;
            IsActive = resource.IsActive;
            Schedules = resource.Schedules.Select(x => (ScheduleDTO<string>)new ScheduleDTO().MapFrom(x)).ToList();

            return this;
        }

        public TrainerEntity MapTo(TrainerEntity resource)
        {
            resource.Id = Id;
            resource.UserId = UserId;
            resource.IsActive = IsActive;

            return resource;
        }
    }
}
