﻿using FitnessCenter.BLL.Generic.Models.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.DAL.EntityFramework.Entities;
using FitnessCenter.DAL.EntityFramework.Identity.Entities;

namespace FitnessCenter.BLL.DTOs
{
    public class UserDTO : UserDTO<string>, IMapper<UserEntity, UserDTO>, IMapper<UserProfileEntity, UserDTO>
    {
        public UserDTO MapFrom(UserEntity resource)
        {
            Id = resource.Id;
            Email = resource.Email;
            if (resource.UserProfile != null)
            {
                Surname = resource.UserProfile.Surname;
                Name = resource.UserProfile.Name;
                Patronymic = resource.UserProfile.Patronymic;
                Birthday = resource.UserProfile.Birthday;
                Address = resource.UserProfile.Address;
                Photo = resource.UserProfile.Photo;
                Language = resource.UserProfile.Language;
                IsBlocked = resource.UserProfile.IsBlocked;
            }

            return this;
        }

        public UserDTO MapFrom(UserProfileEntity resource)
        {
            return MapFrom(resource.User);
        }

        public UserEntity MapTo(UserEntity resource)
        {
            resource.Id = Id;
            resource.UserName = Email;
            resource.Email = Email;

            return resource;
        }

        public UserProfileEntity MapTo(UserProfileEntity resource)
        {
            resource.Id = Id;
            resource.Surname = Surname;
            resource.Name = Name;
            resource.Patronymic = Patronymic;
            resource.Birthday = Birthday;
            resource.Address = Address;
            resource.Photo = Photo;
            resource.Language = Language;
            resource.IsBlocked = IsBlocked;

            return resource;
        }
    }
}
