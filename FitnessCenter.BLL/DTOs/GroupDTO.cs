﻿using FitnessCenter.BLL.Generic.Models.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.DAL.EntityFramework.Entities;
using System.Linq;

namespace FitnessCenter.BLL.DTOs
{
    public class GroupDTO : GroupDTO<string>, IMapper<GroupEntity, GroupDTO>
    {
        public GroupDTO MapFrom(GroupEntity resource)
        {
            Id = resource.Id;
            Name = resource.Name;
            MaxNumberOfPeople = resource.MaxNumberOfPeople;
            Cost = resource.Cost;
            Clients = resource.Clients.Select(x => (ClientDTO<string>)new ClientDTO().MapFrom(x)).ToList();
            Schedules = resource.Schedules.Select(x => (ScheduleDTO<string>)new ScheduleDTO().MapFrom(x)).ToList();

            return this;
        }

        public GroupEntity MapTo(GroupEntity resource)
        {
            resource.Id = Id;
            resource.Name = Name;
            resource.MaxNumberOfPeople = MaxNumberOfPeople;
            resource.Cost = Cost;

            return resource;
        }
    }
}
