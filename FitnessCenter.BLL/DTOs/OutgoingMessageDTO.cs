﻿using FitnessCenter.BLL.Generic.Models.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.DAL.EntityFramework.Entities;

namespace FitnessCenter.BLL.DTOs
{
    public class OutgoingMessageDTO : OutgoingMessageDTO<string>, IMapper<OutgoingMessageEntity, OutgoingMessageDTO>
    {
        public OutgoingMessageDTO MapFrom(OutgoingMessageEntity resource)
        {
            Id = resource.Id;
            UserId = resource.UserId;
            UserSurname = resource.User.UserProfile.Surname;
            UserName = resource.User.UserProfile.Name;
            UserPatronymic = resource.User.UserProfile.Patronymic;
            Title = resource.Title;
            Text = resource.Text;
            SentDate = resource.SentDate;

            return this;
        }

        public OutgoingMessageEntity MapTo(OutgoingMessageEntity resource)
        {
            resource.Id = Id;
            resource.UserId = UserId;
            resource.Title = Title;
            resource.Text = Text;
            resource.SentDate = SentDate;

            return resource;
        }
    }
}
