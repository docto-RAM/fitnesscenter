﻿using FitnessCenter.BLL.Generic.Models.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.DAL.EntityFramework.Entities;

namespace FitnessCenter.BLL.DTOs
{
    public class ClientDTO : ClientDTO<string>, IMapper<ClientEntity, ClientDTO>
    {
        public ClientDTO MapFrom(ClientEntity resource)
        {
            Id = resource.Id;
            UserId = resource.UserId;
            UserSurname = resource.User.UserProfile.Surname;
            UserName = resource.User.UserProfile.Name;
            UserPatronymic = resource.User.UserProfile.Patronymic;
            UserBirthday = resource.User.UserProfile.Birthday;
            UserPhoto = resource.User.UserProfile.Photo;
            UserIsBlocked = resource.User.UserProfile.IsBlocked;
            GroupId = resource.GroupId;
            GroupName = resource.Group.Name;
            GroupCost = resource.Group.Cost;
            PaymentDate = resource.PaymentDate;

            return this;
        }

        public ClientEntity MapTo(ClientEntity resource)
        {
            resource.Id = Id;
            resource.UserId = UserId;
            resource.GroupId = GroupId;
            resource.PaymentDate = PaymentDate;

            return resource;
        }
    }
}
