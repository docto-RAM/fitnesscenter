﻿using FitnessCenter.BLL.Generic.Models.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.DAL.EntityFramework.Entities;

namespace FitnessCenter.BLL.DTOs
{
    public class IncomingMessageDTO : IncomingMessageDTO<string>, IMapper<IncomingMessageEntity, IncomingMessageDTO>
    {
        public IncomingMessageDTO MapFrom(IncomingMessageEntity resource)
        {
            Id = resource.Id;
            OutgoingMessageId = resource.OutgoingMessageId;
            UserId = resource.UserId;
            UserSurname = resource.User.UserProfile.Surname;
            UserName = resource.User.UserProfile.Name;
            UserPatronymic = resource.User.UserProfile.Patronymic;
            ReadDate = resource.ReadDate;

            return this;
        }

        public IncomingMessageEntity MapTo(IncomingMessageEntity resource)
        {
            resource.Id = Id;
            resource.UserId = UserId;
            resource.OutgoingMessageId = OutgoingMessageId;
            resource.ReadDate = ReadDate;

            return resource;
        }
    }
}
