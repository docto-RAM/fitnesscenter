﻿using FitnessCenter.BLL.Generic.Models.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.DAL.EntityFramework.Entities;

namespace FitnessCenter.BLL.DTOs
{
    public class ScheduleDTO : ScheduleDTO<string>, IMapper<ScheduleEntity, ScheduleDTO>
    {
        public ScheduleDTO MapFrom(ScheduleEntity resource)
        {
            Id = resource.Id;
            TrainerId = resource.TrainerId;
            TrainerSurname = resource.Trainer.User.UserProfile.Surname;
            TrainerName = resource.Trainer.User.UserProfile.Name;
            TrainerPatronymic = resource.Trainer.User.UserProfile.Patronymic;
            GroupId = resource.GroupId;
            GroupName = resource.Group.Name;
            GroupMaxNumberOfPeopleInGroup = resource.Group.MaxNumberOfPeople;
            GroupCost = resource.Group.Cost;
            FitnessTypeId = resource.FitnessTypeId;
            FitnessTypeName = resource.FitnessType.Name;
            BeginTime = resource.BeginTime;
            EndTime = resource.EndTime;
            Place = resource.Place;

            return this;
        }

        public ScheduleEntity MapTo(ScheduleEntity resource)
        {
            resource.Id = Id;
            resource.TrainerId = TrainerId;
            resource.GroupId = GroupId;
            resource.BeginTime = BeginTime;
            resource.EndTime = EndTime;
            resource.Place = Place;

            return resource;
        }
    }
}
