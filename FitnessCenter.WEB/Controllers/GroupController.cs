﻿using AutoMapper;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.WEB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using static System.String;

namespace FitnessCenter.WEB.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class GroupController : Controller
    {
        private readonly IClientService<ClientDTO> clientService;
        private readonly IGroupService<GroupDTO> groupService;
        private readonly IUserService<UserDTO, string> userService;

        public GroupController(
            IClientService<ClientDTO> clientService,
            IGroupService<GroupDTO> groupService,
            IUserService<UserDTO, string> userService
        )
        {
            this.clientService = clientService;
            this.groupService = groupService;
            this.userService = userService;
        }

        public async Task<ActionResult> GetGroup(int groupId)
        {
            var groupDTO = await groupService.FindAsync(new GroupDTO { Id = groupId });

            var model = Mapper.Map<GroupViewModel>(groupDTO);
            model.IsAuthenticatedUser = User.Identity.IsAuthenticated;

            if (IsNullOrEmpty(User.Identity.Name))
            {
                return View(model);
            }

            UserDTO userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
            if (userDTO == null)
            {
                return View(model);
            }

            var clientDTOs = clientService.Find(clientDTO => clientDTO.GroupId == groupDTO.Id && clientDTO.UserId == userDTO.Id);
            model.IsUserInGroup = clientDTOs.Count > 0;

            return View(model);
        }

        public ActionResult GetActualGroups(int page = 0)
        {
            var numberOfGroupsForDisplay = (int)AppConstantsEnum.NumberOfGroupsForDisplay;
            var actualGroupsForDisplay = groupService
                .GetAllActual()
                .OrderBy(x => x.Name)
                .Skip(page * numberOfGroupsForDisplay)
                .Take(numberOfGroupsForDisplay);

            return View(Mapper.Map<ICollection<GroupViewModel>>(actualGroupsForDisplay));
        }

        public ActionResult GetActualGroupsPagination()
        {
            return View((int)Math.Ceiling((groupService.GetAllActual()?.Count ?? 0) / (double)AppConstantsEnum.NumberOfGroupsForDisplay));
        }
    }
}