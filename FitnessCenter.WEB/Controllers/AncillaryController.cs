﻿using FitnessCenter.CrossCutting.Ancillary;
using FitnessCenter.CrossCutting.Enumerations;
using System.Web.Mvc;

namespace FitnessCenter.WEB.Controllers
{
    public class AncillaryController : Controller
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeLanguage(LanguageEnum selectedLanguage, string returnUrl)
        {
            Culture.Set(selectedLanguage);

            return RedirectToLocal(returnUrl);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}