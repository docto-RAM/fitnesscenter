﻿using AutoMapper;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Ancillary;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.WEB.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static System.String;

namespace FitnessCenter.WEB.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService<UserDTO, string> userService;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public UserController(IUserService<UserDTO, string> userService)
        {
            this.userService = userService;
        }

        [AllowAnonymous]
        public async Task<ActionResult> Login(string returnUrl)
        {
            if (!IsNullOrEmpty(User.Identity.Name))
            {
                UserDTO userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
                if (userDTO != null)
                {
                    return RedirectToLocal(returnUrl);
                }
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            return await Login(Mapper.Map<UserDTO>(model), () => RedirectToLocal(returnUrl), () => View(model));
        }

        [AllowAnonymous]
        public async Task<ActionResult> Register(string returnUrl)
        {
            if (!IsNullOrEmpty(User.Identity.Name))
            {
                UserDTO userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
                if (userDTO != null)
                {
                    return RedirectToLocal(returnUrl);
                }
            }

            PrepareLanguagesSelectList();
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, HttpPostedFileBase postedFile)
        {
            if (!ModelState.IsValid)
            {
                PrepareLanguagesSelectList();
                return View(model);
            }

            UserDTO userDTO = Mapper.Map<UserDTO>(model);
            userDTO.Photo = GetFile(postedFile);
            userDTO.IsBlocked = false;

            OperationDetails operationDetails = await userService.CreateAsync(userDTO);
            if (!operationDetails.IsSucceeded)
            {
                ModelState.AddModelError(Empty, operationDetails.Message);
                PrepareLanguagesSelectList();
                return View(model);
            }
            await userService.AddToRoleAsync(userDTO, RoleEnum.User);

            return await Login(userDTO, () => RedirectToAction("Index", "Home"), () => 
            {
                PrepareLanguagesSelectList();
                return View(model);
            });
        }

        public ActionResult LogOff()
        {
            Culture.Set(LanguageEnum.English);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        private byte[] GetFile(HttpPostedFileBase postedFile)
        {
            if (postedFile == null)
            {
                return null;
            }

            using (var binaryReader = new BinaryReader(postedFile.InputStream))
            {
                return binaryReader.ReadBytes(postedFile.ContentLength);
            }
        }

        private void PrepareLanguagesSelectList()
        {
            SelectList languages = new SelectList
            (
                Enum.GetNames(typeof(LanguageEnum)).Where(language => !language.Contains("Default"))
            );
            ViewBag.Languages = languages;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private async Task<ActionResult> Login(UserDTO userDto, Func<ActionResult> successfulAction, Func<ActionResult> unsuccessfulAction)
        {
            ClaimsIdentity claimsIdentity = await userService.AuthenticateAsync(userDto);
            if (claimsIdentity == null)
            {
                ModelState.AddModelError(Empty, Resource.UnsuccessfulLoginAttempt);
                return unsuccessfulAction();
            }
            AuthenticationManager.SignOut();
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claimsIdentity);

            UserDTO authenticatedUserDTO = await userService.FindAsync(userDto);
            Culture.Set(authenticatedUserDTO.Language);

            return successfulAction();
        }
    }
}