﻿using AutoMapper;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.WEB.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace FitnessCenter.WEB.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class HomeController : Controller
    {
        IUserService<UserDTO, string> userService;

        public HomeController(IUserService<UserDTO, string> userService)
        {
            this.userService = userService;
        }

        public async Task<ActionResult> Index()
        {
            var model = default(UserViewModel);

            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var user = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
                    if (user == null)
                    {
                        return RedirectToAction("NotFound", "Error");
                    }

                    model = Mapper.Map<UserViewModel>(user);
                    model.Roles = userService.GetRoles(user).OrderBy(x => x.ToString());
                }
            }
            catch
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            return View(model);
        }
    }
}