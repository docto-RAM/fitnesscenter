﻿using FitnessCenter.CrossCutting.Resources;
using System.Web.Mvc;
using System.Web.UI;

namespace FitnessCenter.WEB.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ErrorController : Controller
    {
        public ActionResult BadRequest()
        {
            Response.StatusCode = 400;
            return View("Error", Resource.BadRequest400 as object);
        }

        public ActionResult Forbidden()
        {
            Response.StatusCode = 403;
            return View("Error", Resource.Forbidden403 as object);
        }

        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View("Error", Resource.NotFound404 as object);
        }

        public ActionResult InternalServerError()
        {
            Response.StatusCode = 500;
            return View("Error", Resource.InternalServerError500 as object);
        }
    }
}