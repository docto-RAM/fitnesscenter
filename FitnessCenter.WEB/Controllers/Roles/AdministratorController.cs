﻿using FitnessCenter.CrossCutting.Enumerations;
using System.Web.Mvc;
using System.Web.UI;

namespace FitnessCenter.WEB.Controllers.Roles
{
    [Authorize]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class AdministratorController : Controller
    {
        [Authorize(Roles = nameof(RoleEnum.Administrator))]
        public ActionResult Index()
        {
            return View();
        }
    }
}