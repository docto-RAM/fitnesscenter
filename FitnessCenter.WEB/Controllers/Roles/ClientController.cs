﻿using AutoMapper;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Ancillary;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.WEB.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using static System.DateTime;
using static System.String;

namespace FitnessCenter.WEB.Controllers.Roles
{
    [Authorize]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ClientController : Controller
    {
        private readonly IClientService<ClientDTO> clientService;
        private readonly IGroupService<GroupDTO> groupService;
        private readonly IUserService<UserDTO, string> userService;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ClientController
        (
            IClientService<ClientDTO> clientService,
            IGroupService<GroupDTO> groupService,
            IUserService<UserDTO, string> userService
        )
        {
            this.clientService = clientService;
            this.groupService = groupService;
            this.userService = userService;
        }

        [Authorize(Roles = nameof(RoleEnum.Client))]
        public async Task<ActionResult> Index()
        {
            var currentClientsModel = new List<CurrentClientViewModel>();

            try
            {
                if (IsNullOrEmpty(User.Identity.Name))
                {
                    return RedirectToAction("InternalServerError", "Error");
                }

                var userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
                if (userDTO == null)
                {
                    return RedirectToAction("NotFound", "Error");
                }

                var clientDTOs = clientService.Find(x => x.UserId == userDTO.Id);
                if (clientDTOs.Count == 0)
                {
                    return RedirectToAction("NotFound", "Error");
                }

                foreach (var clientDTO in clientDTOs)
                {
                    var groupDTO = await groupService.FindAsync(new GroupDTO { Id = clientDTO.GroupId });
                    if (!groupDTO.Schedules.Any(x => x.EndTime > Now))
                    {
                        continue;
                    }
                    var groupModel = Mapper.Map<GroupViewModel>(groupDTO);
                    groupModel.IsEnabledLinks = false;

                    currentClientsModel.Add(new CurrentClientViewModel
                    {
                        Id = clientDTO.Id,
                        PaymentDate = clientDTO.PaymentDate,
                        Group = groupModel
                    });
                }
            }
            catch
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            return View(currentClientsModel);
        }

        [Authorize(Roles = nameof(RoleEnum.Client))]
        public async Task<ActionResult> Pay(int clientId)
        {
            try
            {
                var clientDTO = await clientService.FindAsync(new ClientDTO { Id = clientId });
                if (clientDTO.PaymentDate.HasValue)
                {
                    return RedirectToAction("InternalServerError", "Error");
                }
                clientDTO.PaymentDate = Now;
                clientService.Update(clientDTO);
            }
            catch
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(int groupId)
        {
            try
            {
                GroupDTO groupDTO = await groupService.FindAsync(new GroupDTO { Id = groupId });

                if (IsNullOrEmpty(User.Identity.Name))
                {
                    return RedirectToAction("Forbidden", "Error");
                }

                UserDTO userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
                if (userDTO == null)
                {
                    return RedirectToAction("NotFound", "Error");
                }

                var clientDTOs = clientService.Find(clientDTO => clientDTO.GroupId == groupDTO.Id && clientDTO.UserId == userDTO.Id);
                if (clientDTOs.Count > 0)
                {
                    return RedirectToAction("Index");
                }

                OperationDetails operationDetails = await clientService.CreateAsync(new ClientDTO { GroupId = groupDTO.Id, UserId = userDTO.Id });
                if (!operationDetails.IsSucceeded)
                {
                    return RedirectToAction("InternalServerError", "Error");
                }

                if (!userService.IsInRole(userDTO, RoleEnum.Client))
                {
                    await userService.AddToRoleAsync(userDTO, RoleEnum.Client);
                    IdentityResult result = await userService.UpdateSecurityStampAsync(userDTO);
                    if (result != null && result.Succeeded)
                    {
                        ClaimsIdentity claimsIdentity = await userService.AuthenticateAsync(userDTO);
                        if (claimsIdentity == null)
                        {
                            return RedirectToAction("InternalServerError", "Error");
                        }
                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        AuthenticateResult authenticateResult = await AuthenticationManager.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);
                        AuthenticationManager.SignIn(authenticateResult.Properties, claimsIdentity);
                    }
                    else
                    {
                        return RedirectToAction("InternalServerError", "Error");
                    }
                }
            }
            catch
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            return RedirectToAction("Index");
        }
    }
}
