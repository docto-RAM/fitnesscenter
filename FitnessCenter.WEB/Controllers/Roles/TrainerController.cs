﻿using AutoMapper;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.WEB.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using static System.String;

namespace FitnessCenter.WEB.Controllers.Roles
{
    [Authorize]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class TrainerController : Controller
    {
        private readonly IGroupService<GroupDTO> groupService;
        private readonly ITrainerService<TrainerDTO> trainerService;
        private readonly IUserService<UserDTO, string> userService;

        public TrainerController
        (
            IGroupService<GroupDTO> groupService,
            ITrainerService<TrainerDTO> trainerService,
            IUserService<UserDTO, string> userService
        )
        {
            this.groupService = groupService;
            this.trainerService = trainerService;
            this.userService = userService;
        }

        [Authorize(Roles = nameof(RoleEnum.Trainer))]
        public async Task<ActionResult> Index()
        {
            TrainerViewModel model = null;

            try
            {
                if (IsNullOrEmpty(User.Identity.Name))
                {
                    return RedirectToAction("InternalServerError", "Error");
                }

                var userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
                if (userDTO == null)
                {
                    return RedirectToAction("NotFound", "Error");
                }

                var trainerDTOs = trainerService.Find(x => x.UserId == userDTO.Id);
                if (trainerDTOs.Count == 0)
                {
                    return RedirectToAction("NotFound", "Error");
                }

                model = Mapper.Map<TrainerViewModel>(trainerDTOs.First());
            }
            catch
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            return View(model);
        }

        [Authorize(Roles = nameof(RoleEnum.Trainer))]
        public async Task<ActionResult> GetGroup(int groupId)
        {
            var group = await groupService.FindAsync(new GroupDTO { Id = groupId });
            var model = Mapper.Map<GroupViewModel>(group);

            return View(model);
        }
    }
}