﻿using AutoMapper;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Ancillary;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Interfaces.BLL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.WEB.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using static System.DateTime;
using static System.String;

namespace FitnessCenter.WEB.Controllers
{
    [Authorize]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class MessageController : Controller
    {
        private readonly IIncomingMessageService<IncomingMessageDTO> incomingMessageService;
        private readonly IOutgoingMessageService<OutgoingMessageDTO> outgoingMessageService;
        private readonly IUserService<UserDTO, string> userService;

        public MessageController
        (
            IIncomingMessageService<IncomingMessageDTO> incomingMessageService,
            IOutgoingMessageService<OutgoingMessageDTO> outgoingMessageService,
            IUserService<UserDTO, string> userService
        )
        {
            this.incomingMessageService = incomingMessageService;
            this.outgoingMessageService = outgoingMessageService;
            this.userService = userService;
        }

        [Authorize(Roles = nameof(RoleEnum.User))]
        public ActionResult Index(MessageTypeEnum messageType = MessageTypeEnum.Inbox)
        {
            return View(messageType);
        }

        [Authorize(Roles = nameof(RoleEnum.User))]
        public ActionResult Compose()
        {
            return View(new ComposeMessageViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(RoleEnum.User))]
        public async Task<ActionResult> Compose(ComposeMessageViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var resultOfCompose = new ResultOfCompose
            {
                IsSuccess = false,
                Message = Resource.MessageNotSent
            };

            try
            {
                if (IsNullOrEmpty(User.Identity.Name) || IsNullOrEmpty(model.DestinationEmail))
                {
                    return RedirectToAction("ShowResultOfCompose", resultOfCompose);
                }

                UserDTO currentUserDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
                UserDTO destinationUserDTO = await userService.FindAsync(new UserDTO { Email = model.DestinationEmail });
                if (currentUserDTO == null || destinationUserDTO == null)
                {
                    return RedirectToAction("ShowResultOfCompose", resultOfCompose);
                }

                model.UserId = currentUserDTO.Id;
                model.SentDate = Now;
                var outgoingMessageDTO = Mapper.Map<OutgoingMessageDTO>(model);

                OperationDetails operationDetails = await outgoingMessageService.CreateAsync(outgoingMessageDTO);
                if (!operationDetails.IsSucceeded)
                {
                    return RedirectToAction("ShowResultOfCompose", resultOfCompose);
                }

                var outgoingMessageDTOs = outgoingMessageService.Find(message => message.UserId == outgoingMessageDTO.UserId && message.Title == outgoingMessageDTO.Title && message.Text == outgoingMessageDTO.Text);
                if (outgoingMessageDTOs.Count == 0)
                {
                    return RedirectToAction("ShowResultOfCompose", resultOfCompose);
                }

                operationDetails = await incomingMessageService.CreateAsync(new IncomingMessageDTO
                {
                    UserId = destinationUserDTO.Id,
                    OutgoingMessageId = outgoingMessageDTOs.Max(message => message.Id),
                    ReadDate = null
                });
                if (!operationDetails.IsSucceeded)
                {
                    return RedirectToAction("ShowResultOfCompose", resultOfCompose);
                }

                resultOfCompose.IsSuccess = true;
                resultOfCompose.Message = Resource.MessageSent;
            }
            catch
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            return RedirectToAction("ShowResultOfCompose", resultOfCompose);
        }

        [Authorize(Roles = nameof(RoleEnum.User))]
        public ActionResult ShowResultOfCompose(ResultOfCompose model)
        {
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetCountUnreadMessages()
        {
            if (IsNullOrEmpty(User.Identity.Name))
            {
                return View(0);
            }

            UserDTO userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
            if (userDTO == null)
            {
                return View(0);
            }

            var incomingMessageDTOs = incomingMessageService.Find(message => message.UserId == userDTO.Id && !message.ReadDate.HasValue);

            return View(incomingMessageDTOs.Count);
        }

        [Authorize(Roles = nameof(RoleEnum.User))]
        public async Task<ActionResult> GetIncomingMessages()
        {
            if (IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("Forbidden", "Error");
            }
            UserDTO userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
            if (userDTO == null)
            {
                return RedirectToAction("NotFound", "Error");
            }

            var model = new List<IncomingMessageViewModel>();
                
            var incomingMessageDTOs = incomingMessageService.Find(incomingMessage => incomingMessage.UserId == userDTO.Id);
            if (incomingMessageDTOs.Count == 0)
            {
                return View(model);
            }
            var outgoingMessageDTOs = outgoingMessageService.Find(outgoingMessage => incomingMessageDTOs.Select(incomingMessage => incomingMessage.OutgoingMessageId).Contains(outgoingMessage.Id));
            if (outgoingMessageDTOs.Count != incomingMessageDTOs.Count)
            {
                return View(model);
            }

            model = Mapper.Map<List<IncomingMessageViewModel>>(incomingMessageDTOs);
            model.ForEach(incomingMessage => 
            {
                incomingMessage.OutgoingMessage = Mapper.Map<OutgoingMessageViewModel>(outgoingMessageDTOs.Where(outgoingMessage => outgoingMessage.Id == incomingMessage.OutgoingMessageId).FirstOrDefault());
            });

            return View
            (
                model
                    .OrderByDescending(incomingMessage => incomingMessage.OutgoingMessage.SentDate)
                    .ToList()
            );
        }

        [Authorize(Roles = nameof(RoleEnum.User))]
        public async Task<ActionResult> GetOutgoingMessages()
        {
            if (IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("Forbidden", "Error");
            }
            UserDTO userDTO = await userService.FindAsync(new UserDTO { Email = User.Identity.Name });
            if (userDTO == null)
            {
                return RedirectToAction("NotFound", "Error");
            }

            var model = new List<OutgoingMessageViewModel>();
            var outgoingMessageDTOs = outgoingMessageService.Find(message => message.UserId == userDTO.Id);
            if (outgoingMessageDTOs.Count == 0)
            {
                return View(model);
            }
            model = Mapper.Map<List<OutgoingMessageViewModel>>(outgoingMessageDTOs);
            model.ForEach(outgoingMessage => 
            {
                var incomingMessageDTOs = incomingMessageService.Find(message => message.OutgoingMessageId == outgoingMessage.Id);
                outgoingMessage.IncomingMessages = Mapper.Map<List<IncomingMessageViewModel>>(incomingMessageDTOs);
            });

            return View
            (
                model
                    .Where(outgoingMessage => outgoingMessage.IncomingMessages != null && outgoingMessage.IncomingMessages.Count > 0)
                    .OrderByDescending(outgoingMessage => outgoingMessage.SentDate)
                    .ToList()
            );
        }

        [Authorize(Roles = nameof(RoleEnum.User))]
        public async Task MarkMessageAsRead(int messageId)
        {
            var incomingMessageDTO = await incomingMessageService.FindAsync(new IncomingMessageDTO { Id = messageId });
            incomingMessageDTO.ReadDate = Now;
            await incomingMessageService.UpdateAsync(incomingMessageDTO);
        }
    }
}
