﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.DAL.UnitOfWork;

namespace FitnessCenter.WEB.CastleWindsor
{
    public class UnitOfWorkInstaller : IWindsorInstaller
    {
        public UnitOfWorkInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register
            (
                Component
                    .For<IUnitOfWork>()
                    .ImplementedBy<UnitOfWork>()
                    .LifestyleSingleton()
            );
        }
    }
}
