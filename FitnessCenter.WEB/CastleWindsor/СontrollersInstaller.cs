﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace FitnessCenter.WEB.CastleWindsor
{
    public class СontrollersInstaller : IWindsorInstaller
    {
        public СontrollersInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var controllers = Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(x => x.BaseType == typeof(Controller));

            foreach (var controller in controllers)
            {
                container
                    .Register
                    (
                        Component
                            .For(controller)
                            .LifestylePerWebRequest()
                    );
            }
        }
    }
}