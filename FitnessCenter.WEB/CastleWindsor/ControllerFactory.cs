﻿using Castle.Windsor;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace FitnessCenter.WEB.CastleWindsor
{
    public class ControllerFactory : DefaultControllerFactory
    {
        private readonly IWindsorContainer container;

        public ControllerFactory(IWindsorContainer container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        protected override IController GetControllerInstance(RequestContext context, Type controllerType)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (controllerType == null)
            {
                throw new ArgumentNullException(nameof(controllerType));
            }

            return (IController)container.Resolve(controllerType);
        }

        public override void ReleaseController(IController controller)
        {
            var disposableController = controller as IDisposable;
            if (disposableController != null)
            {
                disposableController.Dispose();
            }
            container.Release(controller);
        }
    }
}