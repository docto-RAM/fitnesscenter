﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.DAL.UnitOfWork;

namespace FitnessCenter.WEB.CastleWindsor
{
    public class ConnectionStringInstaller : IWindsorInstaller
    {
        public ConnectionStringInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register
            (
                Component
                    .For<IConnectionStringProvider>()
                    .ImplementedBy<ConnectionStringProvider>()
            );
        }
    }
}