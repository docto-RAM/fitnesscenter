﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using FitnessCenter.BLL.BusinessLogic.Services;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Interfaces.BLL;

namespace FitnessCenter.WEB.CastleWindsor
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public ServicesInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Register
                (
                    Component
                        .For<IClientService<ClientDTO>>()
                        .ImplementedBy<ClientService>()
                )
                .Register
                (
                    Component
                        .For<IGroupService<GroupDTO>>()
                        .ImplementedBy<GroupService>()
                )
                .Register
                (
                    Component
                        .For<IOutgoingMessageService<OutgoingMessageDTO>>()
                        .ImplementedBy<OutgoingMessageService>()
                )
                .Register
                (
                    Component
                        .For<IIncomingMessageService<IncomingMessageDTO>>()
                        .ImplementedBy<IncomingMessageService>()
                )
                .Register
                (
                    Component
                        .For<IScheduleService<ScheduleDTO>>()
                        .ImplementedBy<ScheduleService>()
                )
                .Register
                (
                    Component
                        .For<ITrainerService<TrainerDTO>>()
                        .ImplementedBy<TrainerService>()
                )
                .Register
                (
                    Component
                        .For<IUserService<UserDTO, string>>()
                        .ImplementedBy<UserService>()
                );
        }
    }
}