﻿using System.Web.Optimization;

namespace FitnessCenter.WEB
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js/jquery").Include(
                        "~/FrontEnd/src/js/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/jqueryval").Include(
                        "~/FrontEnd/src/js/jquery/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/js/modernizr").Include(
                        "~/FrontEnd/src/js/modernizr/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/js/bootstrap").Include(
                      "~/FrontEnd/src/js/bootstrap/bootstrap.js",
                      "~/FrontEnd/src/js/bootstrap/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                      "~/FrontEnd/src/js/interface*"));

            bundles.Add(new StyleBundle("~/bundles/assets/styles").Include(
                      "~/FrontEnd/src/assets/styles/bootstrap/bootstrap.css",
                      "~/FrontEnd/src/assets/styles/base.css",
                      "~/FrontEnd/src/assets/styles/less/output/custom-styles.css"));
        }
    }
}