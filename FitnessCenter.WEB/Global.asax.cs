﻿using AutoMapper;
using Castle.Windsor;
using FitnessCenter.BLL.DTOs;
using FitnessCenter.CrossCutting.Ancillary;
using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.WEB.CastleWindsor;
using FitnessCenter.WEB.ViewModels;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FitnessCenter.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Culture.Set(LanguageEnum.English);

            InitializeMapper();
            InitializeServices();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void InitializeMapper()
        {
            Mapper.Initialize(cfg => 
            {
                cfg.CreateMap<ClientViewModel, ClientDTO>();
                cfg.CreateMap<ClientDTO, ClientViewModel>();

                cfg.CreateMap<ComposeMessageViewModel, OutgoingMessageDTO>();
                cfg.CreateMap<OutgoingMessageDTO, ComposeMessageViewModel>();

                cfg.CreateMap<GroupViewModel, GroupDTO>();
                cfg.CreateMap<GroupDTO, GroupViewModel>();

                cfg.CreateMap<IncomingMessageViewModel, IncomingMessageDTO>();
                cfg.CreateMap<IncomingMessageDTO, IncomingMessageViewModel>();

                cfg.CreateMap<LoginViewModel, UserDTO>();

                cfg.CreateMap<OutgoingMessageViewModel, OutgoingMessageDTO>();
                cfg.CreateMap<OutgoingMessageDTO, OutgoingMessageViewModel>();

                cfg.CreateMap<RegisterViewModel, UserDTO>();

                cfg.CreateMap<ScheduleViewModel, ScheduleDTO>();
                cfg.CreateMap<ScheduleDTO, ScheduleViewModel>();

                cfg.CreateMap<TrainerViewModel, TrainerDTO>();
                cfg.CreateMap<TrainerDTO, TrainerViewModel>();

                cfg.CreateMap<UserViewModel, UserDTO>();
                cfg.CreateMap<UserDTO, UserViewModel>();
            });
        }

        private void InitializeServices()
        {
            WindsorContainer windsorContainer = new WindsorContainer();
            windsorContainer.Install
            (
                new ConnectionStringInstaller(),
                new UnitOfWorkInstaller(),
                new ServicesInstaller(),
                new СontrollersInstaller()
            );
            ControllerFactory controllerFactory = new ControllerFactory(windsorContainer);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }
    }
}
