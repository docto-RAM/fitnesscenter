﻿using FitnessCenter.WEB.Generic.Models.ViewModels;
using System.Collections.Generic;

namespace FitnessCenter.WEB.ViewModels
{
    public class TrainerViewModel : TrainerViewModel<string>
    {
        public new ICollection<ScheduleViewModel> Schedules
        {
            get;
            set;
        }
    }
}
