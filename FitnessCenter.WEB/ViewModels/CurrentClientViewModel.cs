﻿using FitnessCenter.WEB.Generic.Models.ViewModels;

namespace FitnessCenter.WEB.ViewModels
{
    public class CurrentClientViewModel : ClientViewModel<string>
    {
        public GroupViewModel Group
        {
            get;
            set;
        }
    }
}