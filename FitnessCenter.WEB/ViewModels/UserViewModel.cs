﻿using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.WEB.Generic.Models.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.ViewModels
{
    public class UserViewModel : UserViewModel<string>
    {
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resource))]
        [Compare("Password", ErrorMessageResourceName = "PasswordsDoNotMatch", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.Password)]
        public string ConfirmPassword
        {
            get;
            set;
        }

        public IEnumerable<RoleEnum> Roles
        {
            get;
            set;
        }
    }
}
