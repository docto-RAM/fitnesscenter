﻿using FitnessCenter.CrossCutting.Resources;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.ViewModels
{
    public class LoginViewModel
    {
        [Display(Name = "Email", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessageResourceName = "EmailAddressIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string Email
        {
            get;
            set;
        }

        [Display(Name = "Password", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }
    }
}