﻿using FitnessCenter.WEB.Generic.Models.ViewModels;
using System.Collections.Generic;

namespace FitnessCenter.WEB.ViewModels
{
    public class OutgoingMessageViewModel : OutgoingMessageViewModel<string>
    {
        public ICollection<IncomingMessageViewModel> IncomingMessages
        {
            get;
            set;
        }
    }
}
