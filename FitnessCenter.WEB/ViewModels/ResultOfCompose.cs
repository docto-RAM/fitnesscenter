﻿namespace FitnessCenter.WEB.ViewModels
{
    public class ResultOfCompose
    {
        public bool IsSuccess
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }
    }
}
