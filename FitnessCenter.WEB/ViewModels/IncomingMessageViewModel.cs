﻿using FitnessCenter.WEB.Generic.Models.ViewModels;

namespace FitnessCenter.WEB.ViewModels
{
    public class IncomingMessageViewModel : IncomingMessageViewModel<string>
    {
        public OutgoingMessageViewModel OutgoingMessage
        {
            get;
            set;
        }
    }
}