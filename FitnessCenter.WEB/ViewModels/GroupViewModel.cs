﻿using FitnessCenter.WEB.Generic.Models.ViewModels;
using System.Collections.Generic;

namespace FitnessCenter.WEB.ViewModels
{
    public class GroupViewModel : GroupViewModel<string>
    {
        public GroupViewModel()
        {
            IsAuthenticatedUser = false;
            IsUserInGroup = false;
            IsEnabledLinks = true;
        }

        public bool IsAuthenticatedUser
        {
            get;
            set;
        }

        public bool IsUserInGroup
        {
            get;
            set;
        }

        public bool IsEnabledLinks
        {
            get;
            set;
        }

        public new ICollection<ClientViewModel> Clients
        {
            get;
            set;
        }

        public new ICollection<ScheduleViewModel> Schedules
        {
            get;
            set;
        }
    }
}
