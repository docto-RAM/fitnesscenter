﻿using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.WEB.Generic.Models.ViewModels;
using System.ComponentModel.DataAnnotations;

namespace FitnessCenter.WEB.ViewModels
{
    public class ComposeMessageViewModel : OutgoingMessageViewModel<string>
    {
        [Display(Name = "To", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "FieldIsRequired", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessageResourceName = "EmailAddressIsInvalid", ErrorMessageResourceType = typeof(Resource))]
        public string DestinationEmail
        {
            get;
            set;
        }
    }
}