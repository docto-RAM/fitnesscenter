﻿"use strict";

function CreateLoader() {
    var self = this;
    var loaderSelector = '#loader';

    this.show = function () {
        $(loaderSelector).show("slow");
    };

    this.hide = function () {
        $(loaderSelector).hide();
    };
};

var Loader = new CreateLoader();

function CreatePayment() {
    var self = this;
    var paymentSelector = '#payment';

    this.show = function () {
        $(paymentSelector).show("slow");
    };

    this.hide = function () {
        $(paymentSelector).hide();
    };
};

var Payment = new CreatePayment();

$(function () {
    $('#actualGroupsPagination')
        .on('loadPagination', function (event) {
            $.get(UrlManager.groupGetActualGroupsPaginationUrl, function (data) {
                event.target.innerHTML = data;
            });
        })
        .on('click', '.page-item:not(.disabled)', function (event) {
            event.preventDefault();
            Loader.show();
            var selectedPageValue = $(this).attr('value');
            $('#actualGroupsPagination .page-item').removeClass('disabled active');
            $('#page' + selectedPageValue).addClass('disabled active');
            $('#previousPage').attr('value', +selectedPageValue - 1);
            if (selectedPageValue <= 0) {
                $('#previousPage').addClass('disabled');
            }
            $('#nextPage').attr('value', +selectedPageValue + 1);
            if (selectedPageValue >= $('input[name=paginationPageCount]').val() - 1) {
                $('#nextPage').addClass('disabled');
            }
            $.get(UrlManager.groupGetActualGroupsUrl, { page: selectedPageValue }, function (data) {
                $('#actualGroups').html(data);
                Loader.hide();
                $("html").animate({ scrollTop: $('#actualGroups').offset().top + 'px' });
            });
        });

    $('#actualGroups')
        .on('loadGroups', function (event) {
            Loader.show();
            $.get(UrlManager.groupGetActualGroupsUrl, function (data) {
                $('#actualGroupsPagination').trigger('loadPagination');
                event.target.innerHTML = data;
                Loader.hide();
            });
        })
        .on('click', '.group-head-incomplete > a', function (event) {
            event.preventDefault();
            Loader.show();
            var groupId = $(this).attr('value');
            $.get(UrlManager.groupGetGroupUrl, { groupId: groupId }, function (data) {
                $("#actualGroups").hide();
                $("#actualGroupsPagination").hide();
                $('#selectedGroup').html(data);
                Loader.hide();
                $("html").animate({ scrollTop: $('#selectedGroup').offset().top + 'px' });
            });
        });

    $('#actualGroups').trigger('loadGroups');

    $('#selectedGroup')
        .on('click', '#backToGroups', function (event) {
            event.preventDefault();
            $('#selectedGroup').empty();
            $("#actualGroups").show();
            $("#actualGroupsPagination").show();
            $("html").animate({ scrollTop: $('#actualGroups').offset().top + 'px' });
        });

    $('#clientPage')
        .on('click', '.text-accent > a', function (event) {
            event.preventDefault();
            $('#clientId').attr('value', $(this).attr('id'));
            $('#cost').text($(this).attr('value'));
            Payment.show();
        })
        .on('click', '#cancelPayment', function (event) {
            event.preventDefault();
            Payment.hide();
            $('#clientId').attr('value', "");
            $('#cost').empty();
        });

    $('#trainerTabs')
        .on('click', 'a', function (event) {
            event.preventDefault();
            $(this).tab('show');
        })
        .on('click', '#classScheduleTab', function (event) {
            event.preventDefault();
            $('#groups').hide();
            $('#classSchedule').show("fast");
        })
        .on('click', '#groupsTab', function (event) {
            event.preventDefault();
            $('#classSchedule').hide();
            $('#groups').show("fast");
        });

    $('#groupSelectionByTrainer')
        .on('change', function () {
            Loader.show();
            var groupId = $(this).val();
            $('#groupSelectionByTrainer option[value=""]').remove();
            $.get(UrlManager.trainerGetGroupUrl, { groupId: groupId }, function (data) {
                $('#selectedGroupByTrainer').html(data);
                Loader.hide();
            });
        });

    $('#countUnreadMessages')
        .on('updateCountUnreadMessages', function (event) {
            $.get(UrlManager.messageGetCountUnreadMessagesUrl, function (data) {
                event.target.innerHTML = data;
            });
        });

    if (!$("div").is('#inbox') && !$("div").is('#sent')) {
        $('#countUnreadMessages').trigger('updateCountUnreadMessages');
    }

    $('#inboxPill')
        .on('click', function (event) {
            event.preventDefault();
            $('#sentPill').removeClass('text-bold');
            $(this).addClass('text-bold');
            $('#sent').hide();
            $('#inbox').trigger('loadIncomingMessages');
            $('#inbox').show("fast");
        });

    $('#sentPill')
        .on('click', function (event) {
            event.preventDefault();
            $('#inboxPill').removeClass('text-bold');
            $(this).addClass('text-bold');
            $('#inbox').hide();
            $('#sent').trigger('loadOutgoingMessages');
            $('#sent').show("fast");
        });

    $('#inbox')
        .on('loadIncomingMessages', function (event) {
            $.get(UrlManager.messageGetIncomingMessagesUrl, function (data) {
                $('#countUnreadMessages').trigger('updateCountUnreadMessages');
                event.target.innerHTML = data;
            });
        });

    $('#inbox:not([class="displayNone"])').trigger('loadIncomingMessages');

    $('#sent')
        .on('loadOutgoingMessages', function (event) {
            $.get(UrlManager.messageGetOutgoingMessagesUrl, function (data) {
                event.target.innerHTML = data;
            });
        });

    $('#sent:not([class="displayNone"])').trigger('loadOutgoingMessages');

    $('#inbox, #sent')
        .on('click', '.messageItem', function (event) {
            event.preventDefault();
            var messageHeaderNode = $(this).children('.messageHeader');
            var messageBodyNode = $(this).children('.messageBody');
            var messageId = messageBodyNode.attr('value');
            if (messageHeaderNode.hasClass('text-bold')) {
                messageHeaderNode.removeClass('text-bold');
                if (messageId !== undefined) {
                    $.get(UrlManager.messageMarkMessageAsReadUrl, { messageId: messageId }, function () {
                        $('#countUnreadMessages').trigger('updateCountUnreadMessages');
                    });
                }
            }
            if (messageBodyNode.hasClass('displayNone')) {
                messageBodyNode.removeClass('displayNone');
            }
            else {
                messageBodyNode.addClass('displayNone');
            }
        });

    $('#selectedLanguage')
        .on('change', function () {
            $('#languageSelectionForm').submit();
        });
});
