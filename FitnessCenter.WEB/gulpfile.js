﻿"use strict";

var gulp = require('gulp');
var less = require('gulp-less');

var paths = {
    webLessRoot: 'FrontEnd/src/assets/styles/less/output/'
};

gulp.task('BuildCustomStyles', function () {
    return gulp.src('FrontEnd/src/assets/styles/less/custom-styles.less')
        .pipe(less())
        .pipe(gulp.dest(paths.webLessRoot));
});