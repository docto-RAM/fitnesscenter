﻿using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework.Entities;
using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using static System.String;

namespace FitnessCenter.DAL.EntityFramework
{
    public class DbContext : IdentityDbContext<UserEntity>
    {
        private static object syncRoot;
        private static string connectionString;
        private static DbContext dbContext;

        static DbContext()
        {
            syncRoot = new Object();
            connectionString = Empty;
            dbContext = null;

            Database.SetInitializer(new DbInitializer());
        }

        public static DbContext GetDbContext(string connectionString)
        {
            if (IsNullOrEmpty(DbContext.connectionString))
            {
                DbContext.connectionString = connectionString;
            }
            else if (!IsNullOrEmpty(DbContext.connectionString) && DbContext.connectionString != connectionString)
            {
                throw new DataAccessException(Resource.InternalServerError500);
            }

            if (dbContext == null)
            {
                lock (syncRoot)
                {
                    if (dbContext == null)
                    {
                        dbContext = new DbContext(DbContext.connectionString);
                    }
                }
            }

            return dbContext;
        }


        public DbSet<UserProfileEntity> UserProfiles
        {
            get;
            set;
        }

        public DbSet<OutgoingMessageEntity> OutgoingMessages
        {
            get;
            set;
        }

        public DbSet<IncomingMessageEntity> IncomingMessages
        {
            get;
            set;
        }

        public DbSet<TrainerEntity> Trainers
        {
            get;
            set;
        }

        public DbSet<ClientEntity> Clients
        {
            get;
            set;
        }

        public DbSet<GroupEntity> Groups
        {
            get;
            set;
        }

        public DbSet<ScheduleEntity> Schedules
        {
            get;
            set;
        }

        public DbSet<FitnessTypeEntity> FitnessTypes
        {
            get;
            set;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Types()
                .Where(type =>
                {
                    return type.Name.EndsWith("Entity");
                })
                .Configure(conventionTypeConfiguration =>
                {
                    conventionTypeConfiguration.ToTable(conventionTypeConfiguration.ClrType.Name.Replace("Entity", "s"));
                });

            base.OnModelCreating(modelBuilder);
        }


        protected DbContext(string conectionString)
            : base(conectionString)
        {
        }
    }
}
