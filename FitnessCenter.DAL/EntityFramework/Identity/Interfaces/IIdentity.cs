﻿using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FitnessCenter.DAL.EntityFramework.Identity.Interfaces
{
    public interface IIdentity
    {
        IdentityResult UpdateSecurityStamp(string userId);
        Task<IdentityResult> UpdateSecurityStampAsync(string userId);
        ClaimsIdentity CreateIdentity(UserEntity user, string authenticationType);
        Task<ClaimsIdentity> CreateIdentityAsync(UserEntity user, string authenticationType);
    }
}