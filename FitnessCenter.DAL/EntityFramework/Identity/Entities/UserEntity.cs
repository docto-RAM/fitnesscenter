﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.DAL.EntityFramework.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace FitnessCenter.DAL.EntityFramework.Identity.Entities
{
    public class UserEntity : IdentityUser, IUserEntity<string>
    {
        public virtual UserProfileEntity UserProfile
        {
            get;
            set;
        }
        
        public virtual ICollection<OutgoingMessageEntity> OutgoingMessages
        {
            get;
            set;
        }

        public virtual ICollection<IncomingMessageEntity> IncomingMessages
        {
            get;
            set;
        }

        public virtual ICollection<ClientEntity> Clients
        {
            get;
            set;
        }

        public virtual ICollection<TrainerEntity> Trainers
        {
            get;
            set;
        }
    }
}
