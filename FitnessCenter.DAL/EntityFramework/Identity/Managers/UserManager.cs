﻿using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using Microsoft.AspNet.Identity;

namespace FitnessCenter.DAL.EntityFramework.Identity.Managers
{
    public class UserManager : UserManager<UserEntity>
    {
        public UserManager(IUserStore<UserEntity> store)
            : base(store)
        {
            UserValidator = new UserValidator<UserEntity>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 4,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
        }
    }
}
