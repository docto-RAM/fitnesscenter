﻿using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FitnessCenter.DAL.EntityFramework.Identity.Managers
{
    public class RoleManager : RoleManager<RoleEntity>
    {
        public RoleManager(RoleStore<RoleEntity> store)
            : base(store)
        {
        }
    }
}
