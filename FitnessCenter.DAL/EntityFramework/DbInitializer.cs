﻿using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Exceptions;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework.Entities;
using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.EntityFramework.Identity.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;
using static System.DateTime;
using static System.String;

namespace FitnessCenter.DAL.EntityFramework
{
    public class DbInitializer : CreateDatabaseIfNotExists<DbContext>
    {
        protected override void Seed(DbContext dbContext)
        {
            UserManager userManager = new UserManager(new UserStore<UserEntity>(dbContext));
            RoleManager roleManager = new RoleManager(new RoleStore<RoleEntity>(dbContext));

            #region Roles

            RoleEntity role0 = new RoleEntity { Name = RoleEnum.User.ToString() };
            RoleEntity role1 = new RoleEntity { Name = RoleEnum.Client.ToString() };
            RoleEntity role2 = new RoleEntity { Name = RoleEnum.Trainer.ToString() };
            RoleEntity role3 = new RoleEntity { Name = RoleEnum.Administrator.ToString() };

            roleManager.Create(role0);
            roleManager.Create(role1);
            roleManager.Create(role2);
            roleManager.Create(role3);

            #endregion

            #region Administrator

            UserEntity administrator = null;
            CreateAdministrator(dbContext, userManager, (x) => administrator = x, role0, role3);
            if (administrator == null)
            {
                throw new UnexpectedResultException(Resource.TheAdministratorInstanceWasNotCreated);
            }

            #endregion

            #region FitnessTypes

            FitnessTypeEntity fitnessType0 = new FitnessTypeEntity { Name = FitnessTypeEnum.Aerobics };
            FitnessTypeEntity fitnessType1 = new FitnessTypeEntity { Name = FitnessTypeEnum.AquaAerobics };
            FitnessTypeEntity fitnessType2 = new FitnessTypeEntity { Name = FitnessTypeEnum.Bodyflex };
            FitnessTypeEntity fitnessType3 = new FitnessTypeEntity { Name = FitnessTypeEnum.Callanetics };
            FitnessTypeEntity fitnessType4 = new FitnessTypeEntity { Name = FitnessTypeEnum.Crossfit };
            FitnessTypeEntity fitnessType5 = new FitnessTypeEntity { Name = FitnessTypeEnum.Pilates };
            FitnessTypeEntity fitnessType6 = new FitnessTypeEntity { Name = FitnessTypeEnum.Shaping };
            FitnessTypeEntity fitnessType7 = new FitnessTypeEntity { Name = FitnessTypeEnum.Stretching };
            FitnessTypeEntity fitnessType8 = new FitnessTypeEntity { Name = FitnessTypeEnum.Workout };
            FitnessTypeEntity fitnessType9 = new FitnessTypeEntity { Name = FitnessTypeEnum.Yoga };

            dbContext.FitnessTypes.Add(fitnessType0);
            dbContext.FitnessTypes.Add(fitnessType1);
            dbContext.FitnessTypes.Add(fitnessType2);
            dbContext.FitnessTypes.Add(fitnessType3);
            dbContext.FitnessTypes.Add(fitnessType4);
            dbContext.FitnessTypes.Add(fitnessType5);
            dbContext.FitnessTypes.Add(fitnessType6);
            dbContext.FitnessTypes.Add(fitnessType7);
            dbContext.FitnessTypes.Add(fitnessType8);
            dbContext.FitnessTypes.Add(fitnessType9);

            #endregion

            #region Groups

            GroupEntity group0 = new GroupEntity { Name = "Gr0023", MaxNumberOfPeople = 18, Cost = 194.23M };
            GroupEntity group1 = new GroupEntity { Name = "Gr0025", MaxNumberOfPeople = 20, Cost = 180.83M };
            GroupEntity group2 = new GroupEntity { Name = "Gr0034", MaxNumberOfPeople = 25, Cost = 204.76M };
            GroupEntity group3 = new GroupEntity { Name = "Gr0037", MaxNumberOfPeople = 16, Cost = 194.00M };
            GroupEntity group4 = new GroupEntity { Name = "Gr0048", MaxNumberOfPeople = 21, Cost = 247.91M };
            GroupEntity group5 = new GroupEntity { Name = "Gr0058", MaxNumberOfPeople = 20, Cost = 189.61M };
            GroupEntity group6 = new GroupEntity { Name = "Gr0063", MaxNumberOfPeople = 18, Cost = 184.23M };
            GroupEntity group7 = new GroupEntity { Name = "Gr0065", MaxNumberOfPeople = 12, Cost = 280.80M };
            GroupEntity group8 = new GroupEntity { Name = "Gr0074", MaxNumberOfPeople = 25, Cost = 214.76M };
            GroupEntity group9 = new GroupEntity { Name = "Gr0097", MaxNumberOfPeople = 16, Cost = 124.01M };
            GroupEntity group10 = new GroupEntity { Name = "Gr0123", MaxNumberOfPeople = 12, Cost = 194.23M };
            GroupEntity group11 = new GroupEntity { Name = "Gr0125", MaxNumberOfPeople = 15, Cost = 180.83M };
            GroupEntity group12 = new GroupEntity { Name = "Gr0134", MaxNumberOfPeople = 15, Cost = 204.76M };
            GroupEntity group13 = new GroupEntity { Name = "Gr0137", MaxNumberOfPeople = 16, Cost = 194.00M };
            GroupEntity group14 = new GroupEntity { Name = "Gr0148", MaxNumberOfPeople = 18, Cost = 247.91M };

            dbContext.Groups.Add(group0);
            dbContext.Groups.Add(group1);
            dbContext.Groups.Add(group2);
            dbContext.Groups.Add(group3);
            dbContext.Groups.Add(group4);
            dbContext.Groups.Add(group5);
            dbContext.Groups.Add(group6);
            dbContext.Groups.Add(group7);
            dbContext.Groups.Add(group8);
            dbContext.Groups.Add(group9);
            dbContext.Groups.Add(group10);
            dbContext.Groups.Add(group11);
            dbContext.Groups.Add(group12);
            dbContext.Groups.Add(group13);
            dbContext.Groups.Add(group14);

            #endregion

            #region Clients

            CreateUsers(16, dbContext, userManager, null, group0, role0, role1);
            CreateUsers(8, dbContext, userManager, null, group1, role0, role1);
            CreateUsers(25, dbContext, userManager, null, group2, role0, role1);
            CreateUsers(11, dbContext, userManager, null, group3, role0, role1);
            CreateUsers(15, dbContext, userManager, null, group4, role0, role1);
            CreateUsers(11, dbContext, userManager, null, group5, role0, role1);
            CreateUsers(18, dbContext, userManager, null, group6, role0, role1);
            CreateUsers(11, dbContext, userManager, null, group7, role0, role1);
            CreateUsers(15, dbContext, userManager, null, group8, role0, role1);
            CreateUsers(11, dbContext, userManager, null, group9, role0, role1);
            CreateUsers(10, dbContext, userManager, null, group10, role0, role1);
            CreateUsers(8, dbContext, userManager, null, group11, role0, role1);
            CreateUsers(15, dbContext, userManager, null, group12, role0, role1);
            CreateUsers(16, dbContext, userManager, null, group13, role0, role1);
            CreateUsers(15, dbContext, userManager, null, group14, role0, role1);

            #endregion

            #region Trainers

            TrainerEntity trainer0 = null;
            CreateUsers(1, dbContext, userManager, (obj) => trainer0 = (obj is TrainerEntity) ? (TrainerEntity)obj : null, true, role0, role2);

            TrainerEntity trainer1 = null;
            CreateUsers(1, dbContext, userManager, (obj) => trainer1 = (obj is TrainerEntity) ? (TrainerEntity)obj : null, true, role0, role2);

            TrainerEntity trainer2 = null;
            CreateUsers(1, dbContext, userManager, (obj) => trainer2 = (obj is TrainerEntity) ? (TrainerEntity)obj : null, true, role0, role2);

            TrainerEntity trainer3 = null;
            CreateUsers(1, dbContext, userManager, (obj) => trainer3 = (obj is TrainerEntity) ? (TrainerEntity)obj : null, true, role0, role2);

            TrainerEntity trainer4 = null;
            CreateUsers(1, dbContext, userManager, (obj) => trainer4 = (obj is TrainerEntity) ? (TrainerEntity)obj : null, true, role0, role2);

            if (
                trainer0 == null ||
                trainer1 == null ||
                trainer2 == null ||
                trainer3 == null ||
                trainer4 == null
                )
            {
                throw new UnexpectedResultException(Resource.OneOrMoreInstancesOfTheTrainerWereNotCreated);
            }

            #endregion

            #region Schedules

            CreateSchedules(3, dbContext, group0, trainer0, fitnessType9, 2, 9, 50);
            CreateSchedules(5, dbContext, group1, trainer1, fitnessType7, 3, 19, 60);
            CreateSchedules(4, dbContext, group2, trainer2, fitnessType3, 4, 10, 60);
            CreateSchedules(4, dbContext, group3, trainer3, fitnessType8, 6, 11, 45);
            CreateSchedules(3, dbContext, group4, trainer2, fitnessType3, -7, 16, 60);
            CreateSchedules(5, dbContext, group5, trainer4, fitnessType5, 9, 18, 60);
            CreateSchedules(3, dbContext, group6, trainer2, fitnessType3, 9, 14, 60);
            CreateSchedules(4, dbContext, group7, trainer4, fitnessType9, 10, 17, 45);
            CreateSchedules(5, dbContext, group8, trainer0, fitnessType3, 10, 16, 60);
            CreateSchedules(5, dbContext, group9, trainer4, fitnessType1, -12, 20, 60);
            CreateSchedules(3, dbContext, group10, trainer0, fitnessType9, 12, 9, 50);
            CreateSchedules(5, dbContext, group11, trainer1, fitnessType7, 13, 19, 60);
            CreateSchedules(4, dbContext, group12, trainer2, fitnessType3, 14, 10, 60);
            CreateSchedules(4, dbContext, group13, trainer3, fitnessType8, 16, 11, 45);
            CreateSchedules(3, dbContext, group14, trainer2, fitnessType3, 17, 16, 60);

            #endregion

            foreach (var user in dbContext.Users)
            {
                user.UserName = user.Email;
            }

            base.Seed(dbContext);
        }

        private void CreateAdministrator(DbContext dbContext, UserManager userManager, Action<UserEntity> action, params RoleEntity[] roles)
        {
            string userName = "nemo";
            string email = "nemo@yandex.ru";
            string password = "Step2019";
            UserEntity administrator = new UserEntity
            {
                UserName = userName,
                Email = email 
            };

            IdentityResult result = userManager.Create(administrator, password);
            if (result.Succeeded)
            {
                foreach (IdentityRole role in roles)
                {
                    userManager.AddToRole(administrator.Id, role.Name);
                }

                UserProfileEntity userProfile = new UserProfileEntity
                {
                    Id = administrator.Id,
                    Surname = "Rokhlikov",
                    Name = "Artur",
                    Patronymic = "Mikhaylovich",
                    Birthday = new DateTime(1981, 12, 3),
                    Address = "121 Mazurova st, Gomel",
                    Language = LanguageEnum.English,
                    IsBlocked = false
                };
                dbContext.UserProfiles.Add(userProfile);

                action?.Invoke(administrator);
            }
        }

        private void CreateUsers(int numberOfUsers, DbContext dbContext, UserManager userManager, Action<object> action, object objectX, params RoleEntity[] roles)
        {
            string password = "Step2019";
            string[] surnames = GetSurnames();
            string[] names = GetNames();
            string[] patronymics = GetPatronymics();
            string сlientName = "client";
            string trainerName = "trainer";
            Random random = new Random();

            while (numberOfUsers-- > 0)
            {
                string surname = surnames[random.Next(0, surnames.Length)];
                string name = names[random.Next(0, names.Length)];
                string patronymic = patronymics[random.Next(0, patronymics.Length)];
                string userName = Empty;
                if (objectX is GroupEntity && !dbContext.Users.Any(x => x.UserName == сlientName))
                {
                    userName = сlientName;
                }
                else if (objectX is bool && !dbContext.Users.Any(x => x.UserName == trainerName))
                {
                    userName = trainerName;
                }
                else
                {
                    userName = $"{surname}{random.Next(0, 1000)}";
                }
                string email = $"{userName}@mail.com";
                UserEntity applicationUser = new UserEntity
                {
                    UserName = userName,
                    Email = email
                };

                IdentityResult identityResult = userManager.Create(applicationUser, password);
                if (identityResult.Succeeded)
                {
                    foreach (RoleEntity role in roles)
                    {
                        if (
                            role.Name == nameof(RoleEnum.User) ||
                            (role.Name == nameof(RoleEnum.Client) && objectX is GroupEntity) ||
                            (role.Name == nameof(RoleEnum.Trainer) && objectX is bool)
                            )
                        {
                            userManager.AddToRole(applicationUser.Id, role.Name);
                        }
                    }

                    UserProfileEntity userProfile = new UserProfileEntity
                    {
                        Id = applicationUser.Id,
                        Surname = surname,
                        Name = name,
                        Patronymic = patronymic,
                        Birthday = new DateTime(1970 + random.Next(0, 37), random.Next(1, 13), random.Next(1, 29)),
                        Address = $"{random.Next(1, 100)} {surname} st, Gomel",
                        Language = (Now.Ticks % 2 == 0) ? LanguageEnum.English : LanguageEnum.Русский,
                        IsBlocked = Now.Ticks % 15 == 0
                    };
                    dbContext.UserProfiles.Add(userProfile);

                    if (objectX is GroupEntity)
                    {
                        ClientEntity client = new ClientEntity
                        {
                            UserId = applicationUser.Id,
                            GroupId = ((GroupEntity)objectX).Id,
                            PaymentDate = (Now.Ticks % 4 != 0) ? (Today - new TimeSpan((int)Now.Ticks % 16, 7, 7, 7)) : (DateTime?)null
                        };
                        dbContext.Clients.Add(client);
                        action?.Invoke(client);
                    }

                    if (objectX is bool)
                    {
                        TrainerEntity trainer = new TrainerEntity
                        {
                            UserId = applicationUser.Id,
                            IsActive = (bool)objectX
                        };
                        dbContext.Trainers.Add(trainer);
                        action?.Invoke(trainer);
                    }
                }
            }
        }

        private void CreateSchedules
        (
            int numberOfSchedules,
            DbContext dbContext,
            GroupEntity group,
            TrainerEntity trainer,
            FitnessTypeEntity fitnessType,
            int daysUntilNextLesson,
            int hourOfBeginLesson,
            int lessonDurationInMinutes
        )
        {
            ScheduleEntity schedule = null;
            int day = daysUntilNextLesson;

            while (numberOfSchedules-- > 0)
            {
                day += (numberOfSchedules % 2 == 0) ? 3 : 4;

                schedule = new ScheduleEntity
                {
                    TrainerId = trainer.Id,
                    GroupId = group.Id,
                    FitnessTypeId = fitnessType.Id,
                    BeginTime = Today + new TimeSpan(day, hourOfBeginLesson, 0, 0),
                    EndTime = Today + new TimeSpan(day, hourOfBeginLesson, lessonDurationInMinutes, 0),
                    Place = "Gym"
                };

                dbContext.Schedules.Add(schedule);
            }
        }

        private string[] GetSurnames()
        {
            return new string[]
            {
                "Johnson",
                "Williams",
                "Jones",
                "Brown",
                "Davis",
                "Miller",
                "Wilson",
                "Moore",
                "Taylor",
                "Anderson",
                "Thomas",
                "Jackson",
                "White",
                "Harris",
                "Martin",
                "Thompson",
                "Garcia",
                "Martinez",
                "Robinson",
                "Clark",
                "Rodriguez",
                "Lewis",
                "Lee",
                "Walker",
                "Hall",
                "Allen",
                "Young",
                "Hernandez",
                "King",
                "Wright",
                "Lopez",
                "Hill",
                "Scott",
                "Green",
                "Adams",
                "Baker",
                "Gonzalez",
                "Nelson",
                "Carter",
                "Mitchell",
                "Perez",
                "Roberts",
                "Turner",
                "Phillips",
                "Campbell",
                "Parker",
                "Evans",
                "Edwards",
                "Collins"
            };
        }

        private string[] GetNames()
        {
            return new string[]
            {
                "Jacob",
                "Michael",
                "Joshua",
                "Matthew",
                "Ethan",
                "Andrew",
                "Daniel",
                "William",
                "Joseph",
                "Christopher",
                "Anthony",
                "Ryan",
                "Nicholas",
                "David",
                "Alexander",
                "Tyler",
                "James",
                "John",
                "Dylan",
                "Nathan",
                "Jonathan",
                "Brandon",
                "Samuel",
                "Christian",
                "Emily",
                "Emma",
                "Madison",
                "Olivia",
                "Hannah",
                "Abigail",
                "Isabella",
                "Ashley",
                "Samantha",
                "Elizabeth",
                "Alexis",
                "Sarah",
                "Grace",
                "Alyssa",
                "Sophia",
                "Lauren",
                "Brianna",
                "Kayla",
                "Natalie",
                "Anna",
                "Jessica",
                "Taylor",
                "Chloe",
                "Hailey",
                "Ava"
            };
        }

        private string[] GetPatronymics()
        {
            return new string[]
            {
                "Anthony",
                "Ryan",
                "Nicholas",
                "David",
                "Alexander",
                "Tyler",
                "James",
                "John",
                "Dylan",
                "Nathan",
                "Jonathan",
                "Brandon",
                "Samuel",
                "Christian",
                "Benjamin",
                "Zachary",
                "Logan",
                "Jose",
                "Noah",
                "Justin",
                "Elijah",
                "Gabriel",
                "Caleb",
                "Kevin",
                "Austin",
                "Robert",
                "Thomas",
                "Connor",
                "Evan",
                "Aidan",
                "Jack",
                "Luke",
                "Jordan",
                "Angel",
                "Isaiah",
                "Isaac",
                "Jason",
                "Jackson",
                "Hunter",
                "Cameron",
                "Gavin",
                "Mason",
                "Aaron",
                "Juan",
                "Kyle",
                "Charles",
                "Luis",
                "Adam",
                "Brian"
            };
        }
    }
}
