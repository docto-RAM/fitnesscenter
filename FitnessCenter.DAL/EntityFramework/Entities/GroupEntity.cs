﻿using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.Generic.Models.Entities;
using System.Collections.Generic;

namespace FitnessCenter.DAL.EntityFramework.Entities
{
    public class GroupEntity : GroupEntity<UserEntity, string>
    {
        public new virtual ICollection<ClientEntity> Clients
        {
            get;
            set;
        }

        public new virtual ICollection<ScheduleEntity> Schedules
        {
            get;
            set;
        }
    }
}
