﻿using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.Generic.Models.Entities;

namespace FitnessCenter.DAL.EntityFramework.Entities
{
    public class ClientEntity : ClientEntity<UserEntity, string>
    {
        public new virtual GroupEntity Group
        {
            get;
            set;
        }
    }
}
