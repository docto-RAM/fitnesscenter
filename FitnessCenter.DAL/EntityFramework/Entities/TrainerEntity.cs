﻿using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.Generic.Models.Entities;
using System.Collections.Generic;

namespace FitnessCenter.DAL.EntityFramework.Entities
{
    public class TrainerEntity : TrainerEntity<UserEntity, string>
    {
        public new virtual ICollection<ScheduleEntity> Schedules
        {
            get;
            set;
        }
    }
}
