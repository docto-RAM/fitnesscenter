﻿using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.Generic.Models.Entities;

namespace FitnessCenter.DAL.EntityFramework.Entities
{
    public class ScheduleEntity : ScheduleEntity<UserEntity, string>
    {
        public new virtual TrainerEntity Trainer
        {
            get;
            set;
        }

        public new virtual GroupEntity Group
        {
            get;
            set;
        }

        public new virtual FitnessTypeEntity FitnessType
        {
            get;
            set;
        }
    }
}
