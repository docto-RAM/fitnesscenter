﻿using FitnessCenter.CrossCutting.Interfaces.DAL;

namespace FitnessCenter.DAL.UnitOfWork
{
    public class ConnectionStringProvider : IConnectionStringProvider
    {
        public string ConnectionString => "FitnessCenter";
    }
}