﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.DAL.EntityFramework;
using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.EntityFramework.Identity.Interfaces;
using FitnessCenter.DAL.EntityFramework.Identity.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static System.String;

namespace FitnessCenter.DAL.UnitOfWork.Repositories
{
    public class UserRepository : IUserRepository<UserEntity, string>, IIdentity
    {
        private readonly DbContext dbContext;
        private readonly UserManager userManager;

        public UserRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
            userManager = new UserManager(new UserStore<UserEntity>(this.dbContext));
        }

        public IQueryable<UserEntity> GetAll()
        {
            return userManager.Users;
        }

        public UserEntity FindById(string userId)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            return userManager.FindById(userId);
        }

        public async Task<UserEntity> FindByIdAsync(string userId)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            return await userManager.FindByIdAsync(userId);
        }

        public UserEntity FindByEmail(string email)
        {
            if (IsNullOrEmpty(email))
            {
                throw new ArgumentException(nameof(email));
            }
            return userManager.FindByEmail(email);
        }

        public async Task<UserEntity> FindByEmailAsync(string email)
        {
            if (IsNullOrEmpty(email))
            {
                throw new ArgumentException(nameof(email));
            }
            return await userManager.FindByEmailAsync(email);
        }

        public UserEntity Find(string userName, string password)
        {
            if (IsNullOrEmpty(userName))
            {
                throw new ArgumentException(nameof(userName));
            }
            if (IsNullOrEmpty(password))
            {
                throw new ArgumentException(nameof(password));
            }
            return userManager.Find(userName, password);
        }

        public async Task<UserEntity> FindAsync(string userName, string password)
        {
            if (IsNullOrEmpty(userName))
            {
                throw new ArgumentException(nameof(userName));
            }
            if (IsNullOrEmpty(password))
            {
                throw new ArgumentException(nameof(password));
            }
            return await userManager.FindAsync(userName, password);
        }

        public IEnumerable<UserEntity> Find(Func<UserEntity, bool> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            return userManager.Users?.Where(predicate)?.ToList();
        }

        public IdentityResult UpdateSecurityStamp(string userId)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            return userManager.UpdateSecurityStamp(userId);
        }

        public async Task<IdentityResult> UpdateSecurityStampAsync(string userId)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            return await userManager.UpdateSecurityStampAsync(userId);
        }

        public ClaimsIdentity CreateIdentity(UserEntity user, string authenticationType)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (IsNullOrEmpty(authenticationType))
            {
                throw new ArgumentException(nameof(authenticationType));
            }
            return userManager.CreateIdentity(user, authenticationType);
        }

        public async Task<ClaimsIdentity> CreateIdentityAsync(UserEntity user, string authenticationType)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (IsNullOrEmpty(authenticationType))
            {
                throw new ArgumentException(nameof(authenticationType));
            }
            return await userManager.CreateIdentityAsync(user, authenticationType);
        }

        public bool AddToRole(string userId, string role) => Perform(userManager.AddToRole, userId, role);

        public async Task<bool> AddToRoleAsync(string userId, string role) => await PerformAsync(userManager.AddToRoleAsync, userId, role);

        public IEnumerable<string> GetRoles(string userId)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            return userManager.GetRoles(userId);
        }

        public async Task<IEnumerable<string>> GetRolesAsync(string userId)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            return await userManager.GetRolesAsync(userId);
        }

        public bool IsInRole(string userId, string role)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            if (IsNullOrEmpty(role))
            {
                throw new ArgumentException(nameof(role));
            }
            return userManager.IsInRole(userId, role);
        }

        public async Task<bool> IsInRoleAsync(string userId, string role)
        {
            if (IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            if (IsNullOrEmpty(role))
            {
                throw new ArgumentException(nameof(role));
            }
            return await userManager.IsInRoleAsync(userId, role);
        }

        public bool RemoveFromRole(string userId, string role) => Perform(userManager.RemoveFromRole, userId, role);

        public async Task<bool> RemoveFromRoleAsync(string userId, string role) => await PerformAsync(userManager.RemoveFromRoleAsync, userId, role);

        public bool Create(UserEntity user, string password) => Perform(userManager.Create, user, password);

        public async Task<bool> CreateAsync(UserEntity user, string password) => await PerformAsync(userManager.CreateAsync, user, password);

        public bool Create(UserEntity user) => Perform(userManager.Create, user);

        public async Task<bool> CreateAsync(UserEntity user) => await PerformAsync(userManager.CreateAsync, user);

        public bool Update(UserEntity user) => Perform(userManager.Update, user);

        public async Task<bool> UpdateAsync(UserEntity user) => await PerformAsync(userManager.UpdateAsync, user);

        public bool Delete(UserEntity user) => Perform(userManager.Delete, user);

        public async Task<bool> DeleteAsync(UserEntity user) => await PerformAsync(userManager.DeleteAsync, user);

        private bool Perform<TP>(Func<TP, IdentityResult> func, TP parameter)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }
            IdentityResult result = func(parameter);
            return result.Succeeded;
        }

        private async Task<bool> PerformAsync<TP>(Func<TP, Task<IdentityResult>> func, TP parameter)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }
            IdentityResult result = await func(parameter);
            return result.Succeeded;
        }

        private bool Perform<TP1, TP2>(Func<TP1, TP2, IdentityResult> func, TP1 parameter1, TP2 parameter2)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }
            if (parameter1 == null)
            {
                throw new ArgumentNullException(nameof(parameter1));
            }
            if (parameter2 == null)
            {
                throw new ArgumentNullException(nameof(parameter2));
            }
            IdentityResult result = func(parameter1, parameter2);
            return result.Succeeded;
        }

        private async Task<bool> PerformAsync<TP1, TP2>(Func<TP1, TP2, Task<IdentityResult>> func, TP1 parameter1, TP2 parameter2)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }
            if (parameter1 == null)
            {
                throw new ArgumentNullException(nameof(parameter1));
            }
            if (parameter2 == null)
            {
                throw new ArgumentNullException(nameof(parameter2));
            }
            IdentityResult result = await func(parameter1, parameter2);
            return result.Succeeded;
        }
    }
}
