﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.CrossCutting.Resources;
using FitnessCenter.DAL.EntityFramework;
using FitnessCenter.DAL.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using SDE = System.Data.Entity;

namespace FitnessCenter.DAL.UnitOfWork.Repositories
{
    public class Repository<TEntity, TEntityId> : IRepository<TEntity, TEntityId>
        where TEntity : class, IBaseEntity<TEntityId>
        where TEntityId : IComparable<TEntityId>
    {
        private readonly DbContext dbContext;
        private readonly SDE.IDbSet<TEntity> dbSet;
        private readonly List<TEntity> dbList;
        private string errorMessage;

        public Repository(DbContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = this.dbContext.Set<TEntity>();
            dbList = GetDbList(this.dbContext);
            errorMessage = string.Empty;
        }

        public IQueryable<TEntity> GetAll()
        {
            return dbList.AsQueryable();
        }

        public TEntity FindById(TEntityId id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            return dbSet.SingleOrDefault(x => x.Id.CompareTo(id) == 0);
        }

        public async Task<TEntity> FindByIdAsync(TEntityId id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            return await SDE.QueryableExtensions.SingleOrDefaultAsync(dbSet, x => x.Id.CompareTo(id) == 0);
        }

        public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            return dbSet.Where(predicate)?.ToList();
        }

        public bool Create(TEntity entity) => Perform(x => dbSet.Add(x), entity);

        public async Task<bool> CreateAsync(TEntity entity) => await PerformAsync(x => dbSet.Add(x), entity);

        public bool Update(TEntity entity) => Perform(x => 
        {
            dbContext.Entry(x).State = SDE.EntityState.Modified;
            return x;
        }, entity);

        public async Task<bool> UpdateAsync(TEntity entity) => await PerformAsync(x =>
        {
            dbContext.Entry(x).State = SDE.EntityState.Modified;
            return x;
        }, entity);

        public bool Delete(TEntity entity) => Perform(x => dbSet.Remove(x), entity);

        public async Task<bool> DeleteAsync(TEntity entity) => await PerformAsync(x => dbSet.Remove(x), entity);

        private bool Perform(Func<TEntity, TEntity> func, TEntity entity)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            try
            {
                return func(entity) != null;
            }
            catch (DbEntityValidationException exception)
            {
                PrepareErrorMessage(exception);
                throw new Exception(errorMessage, exception);
            }
        }

        private async Task<bool> PerformAsync(Func<TEntity, TEntity> func, TEntity entity)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            try
            {
                return await Task.Run(() => func(entity) != null);
            }
            catch (DbEntityValidationException exception)
            {
                PrepareErrorMessage(exception);
                throw new Exception(errorMessage, exception);
            }
        }

        private void PrepareErrorMessage(DbEntityValidationException exception)
        {
            errorMessage = string.Empty;
            foreach (var validationResult in exception.EntityValidationErrors)
            {
                foreach (var validationError in validationResult.ValidationErrors)
                {
                    errorMessage += string.Format(Resource.ErrorMessage, validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                }
            }
        }

        private List<TEntity> GetDbList(DbContext dbContext)
        {
            if (typeof(TEntity) == typeof(UserProfileEntity))
            {
                return dbContext
                    .Set<TEntity>()
                    .Include("User")
                    .ToList();
            }

            if (typeof(TEntity) == typeof(ClientEntity))
            {
                return dbContext
                    .Set<TEntity>()
                    .Include("User")
                    .Include("Group")
                    .Include("Group.Schedules")
                    .ToList();
            }

            if (typeof(TEntity) == typeof(TrainerEntity))
            {
                return dbContext
                    .Set<TEntity>()
                    .Include("User")
                    .Include("Schedules")
                    .Include("Schedules.Group")
                    .Include("Schedules.Group.Clients")
                    .Include("Schedules.FitnessType")
                    .ToList();
            }

            if (typeof(TEntity) == typeof(GroupEntity))
            {
                return dbContext
                    .Set<TEntity>()
                    .Include("Clients")
                    .Include("Schedules")
                    .Include("Schedules.Trainer")
                    .Include("Schedules.FitnessType")
                    .ToList();
            }

            if (typeof(TEntity) == typeof(OutgoingMessageEntity))
            {
                return dbContext
                    .Set<TEntity>()
                    .Include("User")
                    .ToList();
            }

            if (typeof(TEntity) == typeof(IncomingMessageEntity))
            {
                return dbContext
                    .Set<TEntity>()
                    .Include("User")
                    .ToList();
            }

            return dbContext
                .Set<TEntity>()
                .ToList();
        }
    }
}
