﻿using FitnessCenter.CrossCutting.Interfaces.DAL;
using FitnessCenter.DAL.EntityFramework;
using FitnessCenter.DAL.EntityFramework.Identity.Entities;
using FitnessCenter.DAL.UnitOfWork.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FitnessCenter.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext dbContext;
        private readonly Dictionary<string, object> repositories;
        private bool disposed;

        public UnitOfWork(IConnectionStringProvider connectionStringProvider)
        {
            dbContext = DbContext.GetDbContext(connectionStringProvider.ConnectionString);
            repositories = new Dictionary<string, object>();
            disposed = false;
        }

        public IRepository<TEntity, TEntityId> GetRepository<TEntity, TEntityId>()
            where TEntity : class, IBaseEntity<TEntityId>
            where TEntityId : IComparable<TEntityId>
        {
            var entityType = typeof(TEntity);
            var entityIdType = typeof(TEntityId);
            var repositoryType = typeof(Repository<,>);

            return (IRepository<TEntity, TEntityId>)GetRepository(repositoryType, entityType, entityIdType);
        }

        public async Task<IRepository<TEntity, TEntityId>> GetRepositoryAsync<TEntity, TEntityId>()
            where TEntity : class, IBaseEntity<TEntityId>
            where TEntityId : IComparable<TEntityId>
        {
            var entityType = typeof(TEntity);
            var entityIdType = typeof(TEntityId);
            var repositoryType = typeof(Repository<,>);

            return (IRepository<TEntity, TEntityId>) await Task.Run(() => GetRepository(repositoryType, entityType, entityIdType));
        }

        public int Save()
        {
            return dbContext.SaveChanges();            
        }

        public async Task<int> SaveAsync()
        {
            return await dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private object GetRepository(Type repositoryType, Type entityType, Type entityIdType)
        {
            var entityTypeName = entityType.Name;

            if (!repositories.ContainsKey(entityTypeName))
            {
                object repository = entityType != typeof(UserEntity)
                    ? Activator.CreateInstance(repositoryType.MakeGenericType(entityType, entityIdType), dbContext)
                    : new UserRepository(dbContext);

                repositories.Add(entityTypeName, repository);
            }

            return repositories[entityTypeName];
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
                disposed = true;
            }
        }
    }
}
