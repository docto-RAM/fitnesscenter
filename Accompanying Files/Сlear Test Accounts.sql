USE fitnessc_FitnessCenter
GO

DECLARE @clientEmail NVARCHAR(16) = 'client@mail.com'
DECLARE @trainerEmail NVARCHAR(16) = 'trainer@mail.com'
DECLARE @clientRoleId NVARCHAR(128) = (SELECT TOP(1) ANR.Id FROM AspNetRoles ANR WHERE ANR.Name = 'Client')

BEGIN TRANSACTION ClearTestAccounts

    DELETE FROM AspNetUserRoles
    WHERE EXISTS (SELECT * FROM AspNetUsers ANU WHERE ANU.Email IN (@clientEmail, @trainerEmail) AND ANU.Id = UserId)
          AND RoleId = @clientRoleId

    DELETE FROM Clients
    WHERE EXISTS (SELECT * FROM AspNetUsers ANU WHERE ANU.Email IN (@clientEmail, @trainerEmail) AND ANU.Id = UserId)

    DELETE FROM OutgoingMessages
    WHERE EXISTS (SELECT * FROM AspNetUsers ANU WHERE ANU.Email IN (@clientEmail, @trainerEmail) AND ANU.Id = UserId)

    DELETE FROM IncomingMessages
    WHERE EXISTS (SELECT * FROM AspNetUsers ANU WHERE ANU.Email IN (@clientEmail, @trainerEmail) AND ANU.Id = UserId)

COMMIT TRANSACTION ClearTestAccounts
--ROLLBACK TRANSACTION ClearTestAccounts
