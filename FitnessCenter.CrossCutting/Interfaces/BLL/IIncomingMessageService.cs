﻿namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface IIncomingMessageService<TDTO> : IBaseService<TDTO, int>
        where TDTO : IBaseDTO<int>
    {
    }
}
