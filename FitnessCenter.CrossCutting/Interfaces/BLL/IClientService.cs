﻿using System.Collections.Generic;

namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface IClientService<TDTO> : IBaseService<TDTO, int>
        where TDTO : IBaseDTO<int>
    {
        ICollection<TDTO> GetAll();
    }
}
