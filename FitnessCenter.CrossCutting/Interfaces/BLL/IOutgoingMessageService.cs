﻿namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface IOutgoingMessageService<TDTO> : IBaseService<TDTO, int>
        where TDTO : IBaseDTO<int>
    {
    }
}
