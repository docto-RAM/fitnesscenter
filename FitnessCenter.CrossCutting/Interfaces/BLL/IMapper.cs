﻿namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface IMapper<TS, TD> 
    {
        TD MapFrom(TS source);
        TS MapTo(TS source);
    }
}