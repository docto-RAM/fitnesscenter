﻿using FitnessCenter.CrossCutting.Enumerations;
using System;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface IUserService<TUserDTO, TUserId> : IBaseService<TUserDTO, TUserId>
        where TUserDTO : IBaseDTO<TUserId>
        where TUserId : IComparable<TUserId>
    {
        ICollection<TUserDTO> GetAll();
        IdentityResult UpdateSecurityStamp(TUserDTO userDTO);
        Task<IdentityResult> UpdateSecurityStampAsync(TUserDTO userDTO);
        ClaimsIdentity Authenticate(TUserDTO userDTO);
        Task<ClaimsIdentity> AuthenticateAsync(TUserDTO userDTO);
        bool AddToRole(TUserDTO userDTO, RoleEnum role);
        Task<bool> AddToRoleAsync(TUserDTO userDTO, RoleEnum role);
        IEnumerable<RoleEnum> GetRoles(TUserDTO userDTO);
        Task<IEnumerable<RoleEnum>> GetRolesAsync(TUserDTO userDTO);
        bool IsInRole(TUserDTO userDTO, RoleEnum role);
        Task<bool> IsInRoleAsync(TUserDTO userDTO, RoleEnum role);
        bool RemoveFromRole(TUserDTO userDTO, RoleEnum role);
        Task<bool> RemoveFromRoleAsync(TUserDTO userDTO, RoleEnum role);
    }
}
