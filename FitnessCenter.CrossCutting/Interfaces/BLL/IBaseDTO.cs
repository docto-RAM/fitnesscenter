﻿using System;

namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface IBaseDTO<TDTOId>
        where TDTOId : IComparable<TDTOId>
    {
        TDTOId Id
        {
            get;
            set;
        }
    }
}
