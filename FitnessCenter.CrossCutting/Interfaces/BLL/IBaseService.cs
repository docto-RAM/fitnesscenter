﻿using FitnessCenter.CrossCutting.Ancillary;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface IBaseService<TDTO, TDTOId> : IDisposable
        where TDTO : IBaseDTO<TDTOId>
        where TDTOId : IComparable<TDTOId>
    {
        TDTO Find(TDTO itemDTO);
        Task<TDTO> FindAsync(TDTO itemDTO);
        ICollection<TDTO> Find(Func<TDTO, bool> predicate);
        OperationDetails Create(TDTO itemDTO);
        Task<OperationDetails> CreateAsync(TDTO itemDTO);
        OperationDetails Update(TDTO itemDTO);
        Task<OperationDetails> UpdateAsync(TDTO itemDTO);
        OperationDetails Delete(TDTO itemDTO);
        Task<OperationDetails> DeleteAsync(TDTO itemDTO);
    }
}
