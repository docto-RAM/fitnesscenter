﻿using System.Collections.Generic;

namespace FitnessCenter.CrossCutting.Interfaces.BLL
{
    public interface ITrainerService<TDTO> : IBaseService<TDTO, int>
        where TDTO : IBaseDTO<int>
    {
        ICollection<TDTO> GetAll();
    }
}
