﻿namespace FitnessCenter.CrossCutting.Interfaces.DAL
{
    public interface IConnectionStringProvider
    {
        string ConnectionString
        {
            get;
        }
    }
}
