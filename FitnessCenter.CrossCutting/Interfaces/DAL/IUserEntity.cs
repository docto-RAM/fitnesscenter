﻿using System;

namespace FitnessCenter.CrossCutting.Interfaces.DAL
{
    public interface IUserEntity<TUserId> : IBaseEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        string Email
        {
            get;
            set;
        }

        string PasswordHash
        {
            get;
            set;
        }
    }
}
