﻿using System;

namespace FitnessCenter.CrossCutting.Interfaces.DAL
{
    public interface IBaseEntity<TEntityId>
        where TEntityId : IComparable<TEntityId>
    {
        TEntityId Id
        {
            get;
            set;
        }
    }
}