﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FitnessCenter.CrossCutting.Interfaces.DAL
{
    public interface IUserRepository<TUserEntity, TUserId> : IRepository<TUserEntity, TUserId>
        where TUserEntity : IUserEntity<TUserId>
        where TUserId : IComparable<TUserId>
    {
        TUserEntity FindByEmail(string email);
        Task<TUserEntity> FindByEmailAsync(string email);
        TUserEntity Find(string userName, string password);
        Task<TUserEntity> FindAsync(string userName, string password);
        bool AddToRole(TUserId userId, string role);
        Task<bool> AddToRoleAsync(TUserId userId, string role);
        IEnumerable<string> GetRoles(TUserId userId);
        Task<IEnumerable<string>> GetRolesAsync(TUserId userId);
        bool IsInRole(TUserId userId, string role);
        Task<bool> IsInRoleAsync(TUserId userId, string role);
        bool RemoveFromRole(TUserId userId, string role);
        Task<bool> RemoveFromRoleAsync(TUserId userId, string role);
        bool Create(TUserEntity user, string password);
        Task<bool> CreateAsync(TUserEntity user, string password);
    }
}