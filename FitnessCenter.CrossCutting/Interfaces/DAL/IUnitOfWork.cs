﻿using System;
using System.Threading.Tasks;

namespace FitnessCenter.CrossCutting.Interfaces.DAL
{
    public interface IUnitOfWork : IDisposable        
    {
        IRepository<TEntity, TEntityId> GetRepository<TEntity, TEntityId>() 
            where TEntity : class, IBaseEntity<TEntityId>
            where TEntityId : IComparable<TEntityId>;
        Task<IRepository<TEntity, TEntityId>> GetRepositoryAsync<TEntity, TEntityId>()
            where TEntity : class, IBaseEntity<TEntityId>
            where TEntityId : IComparable<TEntityId>;
        int Save();
        Task<int> SaveAsync();
    }
}
