﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessCenter.CrossCutting.Interfaces.DAL
{
    public interface IRepository<TEntity, TEntityId>
        where TEntity : IBaseEntity<TEntityId>
        where TEntityId : IComparable<TEntityId>
    {
        IQueryable<TEntity> GetAll();
        TEntity FindById(TEntityId id);
        Task<TEntity> FindByIdAsync(TEntityId id);
        IEnumerable<TEntity> Find(Func<TEntity, bool> predicate);
        bool Create(TEntity entity);
        Task<bool> CreateAsync(TEntity entity);
        bool Update(TEntity entity);
        Task<bool> UpdateAsync(TEntity entity);
        bool Delete(TEntity entity);
        Task<bool> DeleteAsync(TEntity entity);
    }
}
