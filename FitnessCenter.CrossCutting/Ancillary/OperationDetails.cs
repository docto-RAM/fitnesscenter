﻿namespace FitnessCenter.CrossCutting.Ancillary
{
    public class OperationDetails
    {
        public bool IsSucceeded
        {
            get;
            private set;
        }

        public string Message
        {
            get;
            private set;
        }

        public string Subject
        {
            get;
            private set;
        }

        public OperationDetails(bool isSucceeded, string message = "", string subject = "")
        {
            IsSucceeded = isSucceeded;
            Message = message;
            Subject = subject;
        }
    }
}
