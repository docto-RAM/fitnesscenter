﻿using FitnessCenter.CrossCutting.Enumerations;
using System.Globalization;

namespace FitnessCenter.CrossCutting.Ancillary
{
    public static class Culture
    {
        public static void Set(LanguageEnum languageEnum)
        {
            CultureInfo cultureInfo = new CultureInfo(GetName(languageEnum));
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
        }

        public static LanguageEnum GetLanguage()
        {
            switch (CultureInfo.DefaultThreadCurrentUICulture.Name)
            {
                case "en-US":
                    return LanguageEnum.English;
                case "ru-RU":
                    return LanguageEnum.Русский;
                default:
                    return LanguageEnum.English;
            }
        }

        private static string GetName(LanguageEnum languageEnum)
        {
            switch (languageEnum)
            {
                case LanguageEnum.English:
                    return "en-US";
                case LanguageEnum.Русский:
                    return "ru-RU";
                default:
                    return "en-US";
            }
        }
    }
}