﻿using FitnessCenter.CrossCutting.Enumerations;
using FitnessCenter.CrossCutting.Resources;
using System;
using static System.String;

namespace FitnessCenter.CrossCutting.Extensions
{
    public static class EnumExtension
    {
        public static string GetFitnessTypeName(this FitnessTypeEnum fitnessTypeEnum)
        {
            switch (fitnessTypeEnum)
            {
                case FitnessTypeEnum.Aerobics:
                    return Resource.Aerobics;
                case FitnessTypeEnum.AquaAerobics:
                    return Resource.AquaAerobics;
                case FitnessTypeEnum.Bodyflex:
                    return Resource.Bodyflex;
                case FitnessTypeEnum.Callanetics:
                    return Resource.Callanetics;
                case FitnessTypeEnum.Crossfit:
                    return Resource.Crossfit;
                case FitnessTypeEnum.Pilates:
                    return Resource.Pilates;
                case FitnessTypeEnum.Shaping:
                    return Resource.Shaping;
                case FitnessTypeEnum.Stretching:
                    return Resource.Stretching;
                case FitnessTypeEnum.Workout:
                    return Resource.Workout;
                case FitnessTypeEnum.Yoga:
                    return Resource.Yoga;
                default:
                    return Empty;
            }
        }

        public static string GetDayOfWeekShortName(this DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return Resource.Su;
                case DayOfWeek.Monday:
                    return Resource.Mo;
                case DayOfWeek.Tuesday:
                    return Resource.Tu;
                case DayOfWeek.Wednesday:
                    return Resource.We;
                case DayOfWeek.Thursday:
                    return Resource.Th;
                case DayOfWeek.Friday:
                    return Resource.Fr;
                case DayOfWeek.Saturday:
                    return Resource.Sa;
                default:
                    return Empty;
            }
        }
    }
}
