﻿using System;

namespace FitnessCenter.CrossCutting.Exceptions
{
    public class UnexpectedResultException : ApplicationException
    {
        public UnexpectedResultException(string message)
            : base(message)
        {
        }
    }
}
