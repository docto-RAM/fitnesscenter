﻿using System;

namespace FitnessCenter.CrossCutting.Exceptions
{
    public class ValidationException : ApplicationException
    {
        public string Property
        {
            get;
            protected set;
        }

        public ValidationException(string property, string message) 
            : base(message)
        {
            Property = property;
        }
    }
}
