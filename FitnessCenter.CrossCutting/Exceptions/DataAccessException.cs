﻿using System;

namespace FitnessCenter.CrossCutting.Exceptions
{
    public class DataAccessException : ApplicationException
    {
        public DataAccessException(string message)
            : base(message)
        {
        }
    }
}
