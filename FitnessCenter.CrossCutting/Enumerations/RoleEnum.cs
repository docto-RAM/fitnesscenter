﻿namespace FitnessCenter.CrossCutting.Enumerations
{
    public enum RoleEnum
    {
        Default = 0,
        User = 1,
        Client = 2,
        Trainer = 3,
        Administrator = 4,
        DefaultLimit = 5
    }
}
