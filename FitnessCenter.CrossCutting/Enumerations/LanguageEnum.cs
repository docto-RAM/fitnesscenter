﻿namespace FitnessCenter.CrossCutting.Enumerations
{
    public enum LanguageEnum
    {
        Default = 0,
        English = 1,
        Русский = 2,
        DefaultLimit = 3
    }
}
