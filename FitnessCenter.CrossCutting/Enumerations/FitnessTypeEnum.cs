﻿namespace FitnessCenter.CrossCutting.Enumerations
{
    public enum FitnessTypeEnum
    {
        Default = 0,
        Aerobics = 1,
        AquaAerobics = 2,
        Bodyflex = 3,
        Callanetics = 4,
        Crossfit = 5,
        Pilates = 6,
        Shaping = 7,
        Stretching = 8,
        Workout = 9,
        Yoga = 10,
        DefaultLimit = 11
    }
}
