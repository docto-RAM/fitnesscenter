﻿namespace FitnessCenter.CrossCutting.Enumerations
{
    public enum MessageTypeEnum
    {
        Default = 0,
        Inbox = 1,
        Sent = 2,
        DefaultLimit = 3
    }
}
